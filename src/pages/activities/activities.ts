import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, ModalController,
         ActionSheetController, AlertController, Events } from 'ionic-angular';

import { ActivityFormPage } from '../activity-form/activity-form';
import { DeliveriesPage } from '../deliveries/deliveries';

import { Activities } from '../../providers/activities-service';
import { CurrSectionTab } from '../../providers/currSectionTab-service';
import { Formater } from '../../providers/format-service';

@Component({
    selector: 'page-activities',
    templateUrl: 'activities.html'
})
export class ActivitiesPage {
    activities = [];
    section;

    constructor(public navParams: NavParams, public navCtrl: NavController,
                public viewCtrl: ViewController,
                public modalCtrl: ModalController,
                public actionShCtrl: ActionSheetController,
                public alertCtrl: AlertController,
                public events: Events,
                public activitiesService: Activities,
                public currSectTab: CurrSectionTab) {
        this.section = this.navParams.data;
    }

    ionViewDidLoad() {
        this.getAll();
        /* First calls unsubscribe to clean previous subscriptions
           We could call this from ionViewWillUnload / ionViewWillLeave
           but these are not called when we go back to SubjectPage
           ¿Bug? */
        this.events.unsubscribe('addActivity');
        this.events.unsubscribe('acts:rubricsUpdated');
        this.events.subscribe('addActivity', () => {this.add()});
        this.events.subscribe('acts:rubricsUpdated', () => {this.getAll()});
    }

    ionViewWillEnter() {
        this.currSectTab.setActive('Activities');
    }

    formatDate(dateStr) {
        return Formater.formatDate(dateStr);
    }

    getAll() {
        this.activitiesService.getAll(this.section.section_id)
            .then(activities => {
                this.activities = activities;
            })
    }

    add() {
        let modal = this.modalCtrl.create(ActivityFormPage,
                                          {section: this.section});
        modal.onDidDismiss(data => {
            if(data) {
                this.activitiesService.add(data)
                    .then(() => {this.getAll()})
            }
        });
        modal.present();
    }

    modify(act) {
        let modal = this.modalCtrl.create(ActivityFormPage,
                                          {section: this.section,
                                           oldData: act});
        modal.onDidDismiss(data => {
            if(data) {
                this.activitiesService.update(act.activity_id, data)
                    .then(response => {
                        this.getAll();
                    });
            }
        });
        modal.present();
    }

    delete(act) {
        this.activitiesService.canChangeRubric(act).then(canDelete => {
            if(canDelete) {
                let confirmAlert = this.alertCtrl.create({
                    title: '¿Eliminar actividad?',
                    message: '¿Realmente desea eliminar la actividad ' +
                             act.name + ' y todos los datos asociados a ella?',
                    buttons: [
                        { text: 'Cancelar', handler: () => {} },
                        { text: 'Eliminar', handler: () => {
                            this.activitiesService.delete(act.activity_id)
                            .then(response => { this.getAll(); });
                        }}
                    ]
                });
                confirmAlert.present();
            } else {
                let infoAlert = this.alertCtrl.create({
                    title: '¡No puede eliminar esta actividad!',
                    message: `Existen actividades extra en esta sección y esta
                              actividad es la única regular`,
                    buttons: ['OK']
                });
                infoAlert.present();
            }
        })
    }

    actionsActivity(act) {
        let actionSheet = this.actionShCtrl.create({
            title: act.name,
            buttons: [
                { text: 'Eliminar', role: 'destructive',
                  handler: () => { this.delete(act); } },
                { text: 'Modificar', handler: () => { this.modify(act); } },
                { text: 'Cancelar', role: 'cancel' }
            ]
        });
        actionSheet.present();
    }

    regDeliveries(act) {
        let rootNav = this.navCtrl.parent.parent;
        let prevData = [];
        for(let viewCtrl of rootNav.getViews()) {
            prevData.push(viewCtrl.data);
        }
        rootNav.setRoot(DeliveriesPage,
                        {dataStack: prevData,
                         section_id: this.section.section_id,
                         actData: act});
    }
}
