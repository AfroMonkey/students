import { Component } from '@angular/core';
import { NavController, NavParams, ViewController,
         ModalController } from 'ionic-angular';

import { ScoreFormPage } from '../score-form/score-form';
import { SubjectsPage } from '../subjects/subjects';
import { SubjectPage } from '../subject/subject';
import { SectionPage } from '../section/section';

import { Extraordinary } from '../../providers/extraordinary-service';

@Component({
    selector: 'page-extraordinary',
    templateUrl: 'extraordinary.html'
})
export class ExtraordinaryPage {
    private prevData;
    exts = [];

    constructor(public navCtrl: NavController, public navParams: NavParams,
                public viewCtrl: ViewController,
                public modalCtrl: ModalController,
                public extraordinaryService: Extraordinary) {
        this.prevData = this.navParams.get('dataStack');
        this.exts = this.navParams.get('exts');
    }

    setCustomScore(ext) {
        let extData = {score: ext.ext_score == null ? ''
                                                    : ext.ext_score.toString()};
        let modal = this.modalCtrl.create(ScoreFormPage, {oldData: extData});
        modal.onDidDismiss(data => {
            if(data) {
                if(data.remove) {
                    this.deleteScore(ext);
                } else {
                    this.updateScore(ext, data.score);
                }
            }
        });
        modal.present();
    }

    updateScore(ext, score) {
        let prevScore = ext.ext_score;
        ext.ext_score = score;
        if(prevScore) {
            this.extraordinaryService.update(ext.enrolled_id, score);
        } else {
            this.extraordinaryService.add(ext.enrolled_id, score);
        }
    }

    deleteScore(ext) {
        ext.ext_score = null;
        this.extraordinaryService.delete(ext.enrolled_id);
        // Removes student from the view when extr is no longer required
        if(!ext.valid) {
            this.exts.splice(ext, 1);
        }
    }

    dismiss() {
        this.navCtrl.setPages(
            [
                {
                    page: SubjectsPage
                },
                {
                    page: SubjectPage,
                    params: {subjectData: this.prevData[1].subjectData}
                },
                {
                    page: SectionPage,
                    params: {sectionData: this.prevData[2].sectionData,
                             selectedTab: 0}
                }
            ],
            {animate: true}
        );
    }
}
