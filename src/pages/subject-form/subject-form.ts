import { Component, Renderer } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { NavParams, ViewController } from 'ionic-angular';

@Component({
  selector: 'page-subject-form',
  templateUrl: 'subject-form.html'
})
export class SubjectFormPage {
    private data : FormGroup;
    private oldData: any;

    constructor(public navParams: NavParams, public viewCtrl: ViewController,
                public renderer: Renderer,
                private formBuilder: FormBuilder) {
        this.renderer.setElementClass(viewCtrl.pageRef().nativeElement,
                                      'subject-form-modal', true);
        this.oldData = this.navParams.get('oldData');
        if(typeof this.oldData === 'undefined')
        {
            this.oldData = {subject_key: '', name: ''};
        }

        this.data = this.formBuilder.group({
            subject_key:
                [
                    this.oldData.subject_key,
                    Validators.compose(
                        [
                            Validators.pattern('[A-Z][0-9]{4}'),
                            Validators.required
                        ]
                    )
                ],
            name:
                [
                    this.oldData.name,
                    Validators.required
                ]
        });
    }

    dismiss(data?: any) {
        this.viewCtrl.dismiss(data);
    }
}
