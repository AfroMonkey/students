import { Component } from '@angular/core';
import { NavParams, ViewController, ItemSliding, Events, NavController,
         AlertController, ModalController } from 'ionic-angular';

import { StudentFormPage } from '../student-form/student-form';
import { StudentPage } from '../student/student';
import { ExtraordinaryPage } from '../extraordinary/extraordinary';

import { Students } from '../../providers/students-service';
import { CurrSectionTab } from '../../providers/currSectionTab-service';

@Component({
    selector: 'page-students',
    templateUrl: 'students.html'
})
export class StudentsPage {
    section_id;
    students = [];

    constructor(public navParams: NavParams, public viewCtrl: ViewController,
                public navCtrl: NavController,
                public events: Events,
                public modalCtrl: ModalController,
                public alertCtrl: AlertController,
                public studentsService: Students,
                public currSectTab: CurrSectionTab) {
        this.section_id = this.navParams.data;
    }

    ionViewWillEnter() {
        this.currSectTab.setActive('Students');
    }

    ionViewDidLoad() {
        this.getAll();
        /* First calls unsubscribe to clean previous subscriptions
           We could call this from ionViewWillUnload / ionViewWillLeave
           but these are not called when we go back to SubjectPage
           ¿Bug? */
        this.events.unsubscribe('addStudent');
        this.events.unsubscribe('studs:rubricsUpdated');
        this.events.unsubscribe('studentUpdated');
        this.events.unsubscribe('showExtraordinary');
        this.events.subscribe('addStudent', () => {this.add()});
        this.events.subscribe('studs:rubricsUpdated', () => {this.getAll()});
        this.events.subscribe('studentUpdated', () => {this.getAll()});
        this.events.subscribe('showExtraordinary', () => {this.showExtra()});
    }

    getAll() {
        this.studentsService.getAll(this.section_id)
            .then(students => {
                this.students = students;
            })
    }

    add() {
        let modal = this.modalCtrl.create(StudentFormPage,
                                          {section_id: this.section_id});
        modal.onDidDismiss(data => {
            if(data) {
                this.studentsService.add(data, this.section_id, data.exists)
                    .then(() => {this.getAll()})
            }
        });
        modal.present();
    }

    openStudent(student) {
        let modal = this.modalCtrl.create(StudentPage, {data: student});
        modal.present();
    }

    modifyStudent(student, item: ItemSliding, $event) {
        // fix item-option and openStudent at the same time
        event.stopPropagation();
        let modal = this.modalCtrl.create(StudentFormPage, {oldData: student});
        modal.onDidDismiss(data => {
            if(data) {
                this.studentsService.update(student, data).then(() => {
                    this.getAll();
                })
            }
        });
        modal.present();
        item.close();
    }

    deleteStudent(student, item: ItemSliding, $event) {
        // fix item-option and openStudent at the same time
        event.stopPropagation();
        let confirmAlert = this.alertCtrl.create({
            title: '¿Desmatricular estudiante?',
            message: '¿Realmente desea eliminar a ' + student.name
                     + ` (y a todos sus datos) de esta sección?`,
            buttons: [
                { text: 'Cancelar', handler: () => {} },
                { text: 'Eliminar', handler: () => {
                    this.studentsService.delete(student).then(() => {
                        this.getAll();
                    })
                }}
            ]
        });
        confirmAlert.present();
        item.close();
    }

    showExtra() {
        let rootNav = this.navCtrl.parent.parent;
        let prevData = [];
        for(let viewCtrl of rootNav.getViews()) {
            prevData.push(viewCtrl.data);
        }

        let extStuds = [];
        for(let stud of this.students) {
            if((stud.att_percent >= 65 && stud.att_percent < 80) ||
               (stud.att_percent >= 80 && stud.ord_grade < 60)) {
                stud.valid = true;
                extStuds.push(stud);
            } else if(stud.ext_score != null) {
                stud.valid = false;
                extStuds.push(stud);
            }
        }

        rootNav.setRoot(ExtraordinaryPage,
                        {dataStack: prevData,
                         exts: extStuds});
    }
}
