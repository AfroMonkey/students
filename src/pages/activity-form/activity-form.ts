import { Component, Renderer } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { NavParams, ViewController, ToastController } from 'ionic-angular';

import { Activities } from '../../providers/activities-service';
import { Rubrics } from '../../providers/rubrics-service';

@Component({
  selector: 'page-activity-form',
  templateUrl: 'activity-form.html'
})
export class ActivityFormPage {
    private data: FormGroup;
    private type = 0;
    private canBeExtra: boolean = true;
    private canChangeRubric: boolean = true;
    private oldData: any;
    private section: any;
    private rubrics = [];

    constructor(public navParams: NavParams, public viewCtrl: ViewController,
                public renderer: Renderer,
                public toastCtrl: ToastController,
                private formBuilder: FormBuilder,
                private activitiesService: Activities,
                private rubricsService: Rubrics) {
        this.renderer.setElementClass(viewCtrl.pageRef().nativeElement,
                                      'activity-form-modal', true);
        this.section = this.navParams.get('section');
        this.oldData = this.navParams.get('oldData');
        if(typeof this.oldData === 'undefined') {
            this.oldData = {rubric_id: '', name: '', topic: '', due_date: ''};
        } else {
            this.type = this.oldData.type;
        }

        this.data = this.formBuilder.group({
            rubric_id:
                [this.oldData.rubric_id,
                 Validators.required],
            name:
                [this.oldData.name,
                 Validators.required],
            topic:
                [this.oldData.topic,
                 Validators.required],
            due_date:
                [this.oldData.due_date,
                 Validators.required]
        });

        this.rubricsService.getAll(this.section.section_id)
            .then(rubrics => {
                this.rubrics = rubrics;
            })

        if(this.oldData.rubric_id) {
            this.activitiesService.canChangeRubric(this.oldData)
            .then(canChange => {
                this.canChangeRubric = canChange;
            })
            this.checkExtraActs();
        }
    }

    checkExtraActs() {
        this.rubricsService.numberOfActs(this.data.value.rubric_id, 0)
        .then(numOfNormActs => {
            if(numOfNormActs) {
                if(numOfNormActs == 1 && this.oldData.type == 0 &&
                   this.data.value.rubric_id == this.oldData.rubric_id) {
                    this.canBeExtra = false;
                    if(this.type == 1) this.type = 0;
                } else {
                    this.canBeExtra = true;
                }
            } else {
                this.canBeExtra = false;
                if(this.type == 1) this.type = 0;
            }
        })
    }

    updateType(newType) {
        this.type = this.type == newType ? 0 : newType;
    }

    dismiss(data?: any) {
        if(data && (data.name !== this.oldData.name)) {
            this.activitiesService.exists(this.section.section_id, data.name)
                .then(response => {
                    if(response) {
                        let toast = this.toastCtrl.create({
                            message: '¡La actividad ' + data.name +
                                     ' ya existe dentro de esta sección!',
                            duration: 3000,
                            position: 'bottom'
                        });
                        toast.present();
                    } else {
                        data.type = this.type;
                        this.viewCtrl.dismiss(data);
                    }
                })
        } else {
            if(data) data.type = this.type;
            this.viewCtrl.dismiss(data);
        }
    }
}
