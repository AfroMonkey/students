import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';

import { SubjectsPage } from '../subjects/subjects';
import { SubjectPage } from '../subject/subject';
import { SectionPage } from '../section/section';

import { Attendances } from '../../providers/attendances-service';

@Component({
    selector: 'page-attendance',
    templateUrl: 'attendance.html'
})
export class AttendancePage {
    private prevData;
    private classData;
    private dateStr;
    atts = [];

    constructor(public navCtrl: NavController, public navParams: NavParams,
                public viewCtrl: ViewController,
                public attendancesService: Attendances) {
        this.prevData = this.navParams.get('dataStack');
        this.classData = this.navParams.get('classData');
        this.dateStr = this.navParams.get('dateStr');
    }

    ionViewDidLoad() {
        this.attendancesService.getAll(this.classData.section_id,
                                       this.classData.class_id)
            .then(attendances => {
                this.atts = attendances;
                for(let att of this.atts) {
                    if(!att.type) {
                        att.type = 'A';
                        this.attendancesService.add(this.classData.class_id,
                                                    att);
                    }
                }
            })
    }

    updateAtt(att, type) {
        att.type = type;
        this.attendancesService.update(this.classData.class_id, att);
    }

    dismiss() {
        this.navCtrl.setPages(
            [
                {
                    page: SubjectsPage
                },
                {
                    page: SubjectPage,
                    params: {subjectData: this.prevData[1].subjectData}
                },
                {
                    page: SectionPage,
                    params: {sectionData: this.prevData[2].sectionData}
                }
            ],
            {animate: true}
        );
    }
}
