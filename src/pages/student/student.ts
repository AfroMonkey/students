import { Component } from '@angular/core';
import { NavParams, ViewController, Events } from 'ionic-angular';

import { StudentAttendancePage } from '../student-attendance/student-attendance';
import { StudentActivitiesPage } from '../student-activities/student-activities';

@Component({
    selector: 'page-student',
    templateUrl: 'student.html'
})
export class StudentPage {
    data: any;
    modified = false;

    attendanceTab = StudentAttendancePage;
    activitiesTab = StudentActivitiesPage;

    constructor(public navParams: NavParams, public viewCtrl: ViewController,
                public events: Events) {
        this.data = navParams.get('data');
    }

    ionViewDidLoad() {
        this.events.subscribe('studentModified', () => {this.modified = true})
    }

    ionViewWillUnload() {
        this.events.unsubscribe('studentModified');
        if(this.modified) this.events.publish('studentUpdated');
    }

    dismiss() {
        this.viewCtrl.dismiss();
    }
}
