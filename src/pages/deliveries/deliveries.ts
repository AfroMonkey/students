import { Component } from '@angular/core';
import { NavController, NavParams, ViewController,
         ModalController } from 'ionic-angular';

import { ScoreFormPage } from '../score-form/score-form';
import { SubjectsPage } from '../subjects/subjects';
import { SubjectPage } from '../subject/subject';
import { SectionPage } from '../section/section';

import { Deliveries } from '../../providers/deliveries-service';

@Component({
    selector: 'page-deliveries',
    templateUrl: 'deliveries.html'
})
export class DeliveriesPage {
    private prevData;
    private actData;
    private section_id;
    delivs = [];

    constructor(public navCtrl: NavController, public navParams: NavParams,
                public viewCtrl: ViewController,
                public modalCtrl: ModalController,
                public deliveriesService: Deliveries) {
        this.prevData = this.navParams.get('dataStack');
        this.actData = this.navParams.get('actData');
        this.section_id = this.navParams.get('section_id');
    }

    ionViewDidLoad() {
        this.deliveriesService.getAll(this.section_id, this.actData.activity_id)
            .then(deliveries => {
                this.delivs = deliveries;
            })
    }

    setCustomScore(dev) {
        let modal = this.modalCtrl.create(ScoreFormPage, {oldData: dev});
        modal.onDidDismiss(data => {
            if(data) {
                if(data.remove) {
                    this.deleteScore(dev);
                } else {
                    this.updateScore(dev, data.score);
                }
            }
        });
        modal.present();
    }

    updateScore(dev, score) {
        let prevScore = dev.score;
        dev.score = score;
        if(prevScore) {
            this.deliveriesService.update(this.actData.activity_id, dev);
        } else {
            this.deliveriesService.add(this.actData.activity_id, dev);
        }
    }

    deleteScore(dev) {
        dev.score = '';
        this.deliveriesService.delete(this.actData.activity_id, dev.enrolled_id)
    }

    dismiss() {
        this.navCtrl.setPages(
            [
                {
                    page: SubjectsPage
                },
                {
                    page: SubjectPage,
                    params: {subjectData: this.prevData[1].subjectData}
                },
                {
                    page: SectionPage,
                    params: {sectionData: this.prevData[2].sectionData,
                             selectedTab: 2}
                }
            ],
            {animate: true}
        );
    }
}
