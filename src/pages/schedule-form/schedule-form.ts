import { Component, Renderer } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { NavParams, ViewController } from 'ionic-angular';

@Component({
  selector: 'page-schedule-form',
  templateUrl: 'schedule-form.html'
})
export class ScheduleFormPage {
    private data : FormGroup;
    private oldData: any;

    constructor(public navParams: NavParams, public viewCtrl: ViewController,
                public renderer: Renderer, private formBuilder: FormBuilder) {
        this.renderer.setElementClass(viewCtrl.pageRef().nativeElement,
                                      'schedule-modal', true);

        this.oldData = this.navParams.get('oldData');
        if(typeof this.oldData === 'undefined')
        {
            this.oldData = {start: '', end: ''};
        }

        this.data = this.formBuilder.group({
            start:
                [this.oldData.start,
                 Validators.required],
            end:
                [this.oldData.end,
                 Validators.required]
        });
    }

    num2Time(n): String {
        let timeStr = n.toString() + ':00';
        if(n < 10) timeStr = '0' + timeStr;
        return timeStr;
    }

    calcEnd(start): String {
        start += 2;
        if(start > 21) start = 21;
        return this.num2Time(start);
    }

    calcStart(end): String {
        end -= 2;
        if(end < 7) end = 7;
        return this.num2Time(end);
    }

    updateEnd() {
        let start = Number(this.data.value.start.substr(0, 2));
        if(!this.data.value.end) {
            this.data.patchValue({end: this.calcEnd(start)});
        } else {
            let end = Number(this.data.value.end.substr(0, 2));
            if(end <= start) {
                this.data.patchValue({end: this.calcEnd(start)});
            }
        }
    }

    updateStart() {
        let end = Number(this.data.value.end.substr(0, 2));
        if(!this.data.value.start) {
            this.data.patchValue({start: this.calcStart(end)});
        } else {
            let start = Number(this.data.value.start.substr(0, 2));
            if(start >= end) {
                this.data.patchValue({start: this.calcStart(end)});
            }
        }
    }

    remove() {
        this.viewCtrl.dismiss({remove: true});
    }

    dismiss(data?: any) {
        this.viewCtrl.dismiss(data);
    }
}
