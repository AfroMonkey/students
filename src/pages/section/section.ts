import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, Events,
         ModalController } from 'ionic-angular';

import { RubricsPage } from '../rubrics/rubrics';
import { StudentsPage } from '../students/students';
import { ClassesPage } from '../classes/classes';
import { ActivitiesPage } from '../activities/activities';

import { CurrSection } from '../../providers/currSection-service';
import { CurrSectionTab } from '../../providers/currSectionTab-service';

@Component({
    selector: 'page-section',
    templateUrl: 'section.html'
})

export class SectionPage {
    data: any;
    selectedTab;

    studentsTab = StudentsPage;
    classesTab = ClassesPage;
    activitiesTab = ActivitiesPage;

    constructor(public navCtrl: NavController, public navParams: NavParams,
                public viewCtrl: ViewController,
                public events: Events,
                public modalCtrl: ModalController,
                public currSection: CurrSection,
                public currSectTab: CurrSectionTab) {
        this.data = navParams.get('sectionData');
        this.selectedTab = navParams.get('selectedTab');
        if(typeof this.selectedTab === 'undefined') this.selectedTab = 1;
    }

    ionViewWillEnter() {
        this.viewCtrl.setBackButtonText('');
        this.currSection.setActive(true);
        this.currSection.setData(this.data);
    }

    ionViewWillLeave() {
        this.currSection.setActive(false);
    }

    add() {
        if(this.currSectTab.getActive() == 'Students') {
            this.events.publish('addStudent');
        } else {
            this.events.publish('addActivity');
        }
    }

    configRubrics() {
        let modal = this.modalCtrl.create(RubricsPage,
                                          {section_id: this.data.section_id});
        modal.present();
    }

    showExtraordinary() {
        this.events.publish('showExtraordinary');
    }
}
