import { Component } from '@angular/core';
import { NavParams, ViewController, ModalController,
         Events } from 'ionic-angular';

import { ScoreFormPage } from '../score-form/score-form';

import { Deliveries } from '../../providers/deliveries-service';

@Component({
    selector: 'page-student-activities',
    templateUrl: 'student-activities.html'
})
export class StudentActivitiesPage {
    enrolled_id;
    acts = [];

    constructor(public navParams: NavParams, public viewCtrl: ViewController,
                public modalCtrl: ModalController,
                public deliveriesService: Deliveries,
                public events: Events) {
        this.enrolled_id = this.navParams.data;
    }

    ionViewDidLoad() {
        this.deliveriesService.getAllByEnrolled(this.enrolled_id)
            .then(acts => {
                this.acts = acts;
            })
    }

    updateAct(act) {
        let modal = this.modalCtrl.create(ScoreFormPage, {oldData: act});
        modal.onDidDismiss(data => {
            if(data) {
                this.events.publish('studentModified');
                if(data.remove) {
                    act.delivered = 0;
                    act.score = '';
                    this.deliveriesService.delete(act.activity_id,
                                                  this.enrolled_id);
                } else {
                    act.score = data.score;
                    act.enrolled_id = this.enrolled_id;
                    if(act.delivered) {
                        this.deliveriesService.update(act.activity_id, act);
                    } else {
                        this.deliveriesService.add(act.activity_id, act);
                    }
                    act.delivered = 1;
                }
            }
        });
        modal.present();
    }
}
