import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, ViewController,
         ModalController, ActionSheetController, Content } from 'ionic-angular';

import { ClassFormPage } from '../class-form/class-form';
import { AttendancePage } from '../attendance/attendance';

import { Classes } from '../../providers/classes-service';
import { CurrSectionTab } from '../../providers/currSectionTab-service';
import { Formater } from '../../providers/format-service';

@Component({
    selector: 'page-classes',
    templateUrl: 'classes.html'
})
export class ClassesPage {
    @ViewChild(Content) content: Content;
    section_id;
    classes = [];
    todayStr = '';
    todayOffsetStr = ''; // Offset = -2 in order to fit view
    engMonths = {'Jan': '01', 'Feb': '02', 'Mar': '03', 'Apr': '04',
                 'May': '05', 'Jun': '06', 'Jul': '07', 'Aug': '08',
                 'Sep': '09', 'Oct': '10', 'Nov': '11', 'Dec': '12'};

    constructor(public navCtrl: NavController, public navParams: NavParams,
                public viewCtrl: ViewController,
                public modalCtrl: ModalController,
                public actionShCtrl: ActionSheetController,
                public classesService: Classes,
                public currSectTab: CurrSectionTab) {
        this.section_id = this.navParams.data;
    }

    ionViewDidLoad() {
        this.getAll();
    }

    ionViewWillEnter() {
        this.currSectTab.setActive('Classes');
    }

    getAll() {
        this.classesService.getAll(this.section_id)
            .then(classes => {
                this.classes = classes;
            })
    }

    checkToday() {
        let currDate = new Date().toString().split(" "); // Mon Sep 28 1998 ...
        this.todayStr = currDate[3] + '-' + this.engMonths[currDate[1]] + '-' +
                        currDate[2];

        for(let i = 0; i < this.classes.length; ++i) {
            if(this.classes[i].date === this.todayStr) {
                this.todayOffsetStr = i > 1 ? this.classes[i - 2].date
                                            : this.classes[0].date;
                break;
            }
        }
    }

    initScroll() {
        if(this.todayOffsetStr) {
            document.getElementById(this.todayOffsetStr).scrollIntoView();
            document.getElementById(this.todayStr).className = 'today';
        }
    }

    formatDate(dateStr) {
        return Formater.formatDate(dateStr);
    }

    clicked(taughtClass) {
        if(taughtClass.type) {
            if(taughtClass.type == 'N') {
                this.takeAttendance(taughtClass);
            }
        } else {
            this.setClassData(taughtClass, {});
        }
    }

    setClassData(taughtClass, oldDataObj) {
        let modal = this.modalCtrl.create(ClassFormPage, oldDataObj);
        modal.onDidDismiss(data => {
            if(data) {
                this.classesService.setData(taughtClass.class_id, data)
                    .then(response => { this.getAll(); });
            }
        });
        modal.present();
    }

    actionsClass(taughtClass) {
        let actionSheet = this.actionShCtrl.create({
            title: this.formatDate(taughtClass.date),
            buttons: [
                { text: 'Modificar', handler: () => {
                    this.setClassData(taughtClass, {oldData: taughtClass});
                    }
                },
                { text: 'Cancelar', role: 'cancel' }
            ]
        });
        actionSheet.present();
    }

    takeAttendance(taughtClass) {
        let rootNav = this.navCtrl.parent.parent;
        let prevData = [];
        for(let viewCtrl of rootNav.getViews()) {
            prevData.push(viewCtrl.data);
        }
        rootNav.setRoot(AttendancePage,
                        {dataStack: prevData,
                         classData: taughtClass,
                         dateStr: this.formatDate(taughtClass.date)});
    }

    scrollToRelevant() {
        // let yOffset = document.getElementById(this.todayOffsetStr).offsetTop;
        // this.content.scrollTo(0, yOffset, 1500);
    }
}
