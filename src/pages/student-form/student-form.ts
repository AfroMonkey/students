import { Component, Renderer } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { NavParams, ViewController, ToastController } from 'ionic-angular';

import { Students } from '../../providers/students-service';

@Component({
  selector: 'page-student-form',
  templateUrl: 'student-form.html'
})
export class StudentFormPage {
    private section_id;
    private data : FormGroup;
    private oldData: any;
    private exists: boolean;
    private disableName: boolean = false;

    constructor(public navParams: NavParams, public viewCtrl: ViewController,
                public renderer: Renderer,
                private formBuilder: FormBuilder,
                private toastCtrl: ToastController,
                private studentsService: Students) {
        this.renderer.setElementClass(viewCtrl.pageRef().nativeElement,
                                      'student-form-modal', true);
        this.section_id = this.navParams.get('section_id');
        this.oldData = this.navParams.get('oldData');
        if(typeof this.oldData === 'undefined')
        {
            this.oldData = {student_code: '', name: ''};
        }

        this.data = this.formBuilder.group({
            student_code:
                [this.oldData.student_code,
                 Validators.compose([Validators.pattern('[0-9]{9}'),
                                     Validators.required])],
            name:
                [this.oldData.name, Validators.required]
        });
    }

    searchStudent() {
        this.studentsService.name(this.data.value.student_code)
            .then(studName => {
                if(studName) {
                    this.data.patchValue({name: studName});
                    this.disableName = this.oldData.student_code !=
                                       this.data.value.student_code;
                    this.exists = true;
                } else {
                    this.disableName = false;
                    this.exists = false;
                }
            })
    }

    dismiss(data?: any) {
        if(data && (data.student_code !== this.oldData.student_code)) {
            data.exists = this.exists;
            this.studentsService.isEnrolled(data.student_code, this.section_id)
                .then(response => {
                    if(response) {
                        let toast = this.toastCtrl.create({
                            message: '¡El estudiante con el código ' +
                                     data.student_code + ` ya está registrado
                                     en esta sección!`,
                            duration: 3000,
                            position: 'bottom'
                        });
                        toast.present();
                    } else {
                        this.viewCtrl.dismiss(data);
                    }
                })
        } else {
            this.viewCtrl.dismiss(data);
        }
    }
}
