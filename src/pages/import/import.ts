import { Component } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { NavParams, ViewController } from 'ionic-angular';

@Component({
  selector: 'page-import',
  templateUrl: 'import.html'
})
export class ImportPage {
    private data: FormGroup;

    constructor(public navParams: NavParams, public viewCtrl: ViewController,
                public formBuilder: FormBuilder) {
        this.data = this.formBuilder.group({
            studentsStr: ['', Validators.required]
        });
    }

    dismiss(data?: any) {
        this.viewCtrl.dismiss(data);
    }
}
