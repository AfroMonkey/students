import { Component } from '@angular/core';
import { NavController, NavParams, Nav,
         ModalController, AlertController,
         ActionSheetController } from 'ionic-angular';

import { SubjectFormPage } from '../subject-form/subject-form';
import { SubjectPage } from '../subject/subject';

import { Subjects } from '../../providers/subjects-service';

@Component({
  selector: 'page-subjects',
  templateUrl: 'subjects.html'
})
export class SubjectsPage {
    subjects: any[];

    constructor(public navCtrl: NavController, public navParams: NavParams,
                public nav: Nav,
                public modalCtrl: ModalController,
                public alertCtrl: AlertController,
                public actionShCtrl: ActionSheetController,
                public subjectsService: Subjects) {}

    ionViewDidLoad() {
        this.getAll();
        this.nav.swipeBackEnabled = false;
    }

    getAll() {
        this.subjectsService.getAll()
            .then(subjects => {
                this.subjects = subjects;
            })
    }

    add() {
        let modal = this.modalCtrl.create(SubjectFormPage);
        modal.onDidDismiss(data => {
            if(data) {
                Promise.all([
                    this.subjectsService.exists('subject_key', data.subject_key),
                    this.subjectsService.exists('name', data.name)
                ]).then(values => {
                    if(values[0] || values[1]) {
                        let alert = this.alertCtrl.create({
                            title: '¡La materia ya existe!',
                            message: 'Ya se encuentra registrada esa materia',
                            buttons: ['OK']
                        });
                        alert.present();
                    } else {
                        this.subjectsService.add(data)
                            .then(response => {
                                this.getAll();
                            });
                    }
                });
            }
        });
        modal.present();
    }

    modify(subject) {
        let modal = this.modalCtrl.create(SubjectFormPage, {oldData: subject});
        modal.onDidDismiss(data => {
            if(data) {
                Promise.all([
                    this.subjectsService.exists('subject_key', data.subject_key),
                    this.subjectsService.exists('name', data.name)
                ]).then(values => {
                    if((values[0] && data.subject_key !== subject.subject_key)
                       || (values[1] && data.name !== subject.name)) {
                        let alert = this.alertCtrl.create({
                            title: '¡La materia ya existe!',
                            message: 'Ya se encuentra registrada esa materia',
                            buttons: ['OK']
                        });
                        alert.present();
                    } else {
                        this.subjectsService.update(subject.subject_key,
                                                    data)
                            .then(response => {
                                this.getAll();
                            });
                    }
                });
            }
        });
        modal.present();
    }

    delete(subject) {
        let confirmAlert = this.alertCtrl.create({
            title: '¿Eliminar materia?',
            message: '¿Realmente desea eliminar la materia ' +
                      subject.subject_key + ` y todos los datos asociados
                      a ella?`,
            buttons: [
                { text: 'Cancelar', handler: () => {} },
                { text: 'Eliminar', handler: () => {
                    this.subjectsService.delete(subject.subject_key).then(
                        response => { this.getAll(); }
                    );
                }}
            ]
        });
        confirmAlert.present();
    }

    openSubject(subject) {
        this.navCtrl.push(SubjectPage, {subjectData : subject});
    }

    actionsSubject(subject) {
        let actionSheet = this.actionShCtrl.create({
            title: subject.subject_key,
            buttons: [
                { text: 'Eliminar', role: 'destructive',
                  handler: () => { this.delete(subject); } },
                { text: 'Modificar', handler: () => { this.modify(subject); } },
                { text: 'Cancelar', role: 'cancel' }
            ]
        });
        actionSheet.present();
    }
}
