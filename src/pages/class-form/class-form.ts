import { Component, Renderer } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { NavParams, ViewController } from 'ionic-angular';

@Component({
  selector: 'page-class-form',
  templateUrl: 'class-form.html'
})
export class ClassFormPage {
    private data : FormGroup;
    private oldData: any;

    constructor(public navParams: NavParams, public viewCtrl: ViewController,
                public renderer: Renderer,
                private formBuilder: FormBuilder) {
        this.renderer.setElementClass(viewCtrl.pageRef().nativeElement,
                                      'class-form-modal', true);
        this.oldData = this.navParams.get('oldData');
        if(typeof this.oldData === 'undefined')
        {
            this.oldData = {type: '', topic: '', instrument: '', method: ''};
        }

        this.data = this.formBuilder.group({
            type: [this.oldData.type, Validators.required],
            topic: [this.oldData.topic, Validators.required],
            instrument: [this.oldData.instrument, Validators.required],
            method: [this.oldData.method, Validators.required]
        });
    }

    dismiss(data?: any) {
        this.viewCtrl.dismiss(data);
    }

    formIsValid() {
        return (this.data.value.type && this.data.value.type !== 'N') ||
               this.data.valid;
    }
}
