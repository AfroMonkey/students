import { Component } from '@angular/core';
import { NavParams, ViewController, Events } from 'ionic-angular';

import { Attendances } from '../../providers/attendances-service';
import { Formater } from '../../providers/format-service';

@Component({
    selector: 'page-student-attendance',
    templateUrl: 'student-attendance.html'
})
export class StudentAttendancePage {
    enrolled_id;
    atts = [];
    stats = {total: 0, A: 0, R: 0, F: 0};
    typeButtons = 7;

    constructor(public navParams: NavParams, public viewCtrl: ViewController,
                public attendancesService: Attendances,
                public events: Events) {
        this.enrolled_id = this.navParams.data;
    }

    ionViewDidLoad() {
        this.attendancesService.getAllByEnrolled(this.enrolled_id)
            .then(atts => {
                this.atts = atts;
            })
            .then(() => {
                this.attendancesService.getStatsByEnrolled(this.enrolled_id)
                    .then(stats => {
                        this.stats = stats;
                        if(!this.stats.total) this.stats.total = 0;
                    })
            })
    }

    formatDate(dateStr) {
        return Formater.formatDate(dateStr);
    }

    checkFlag(number, powerOf2) {
        return number & powerOf2;
    }

    getStat(type) {
        return Math.floor(this.stats[type] / this.stats.total * 100);
    }

    filterClicked(powerOf2) {
        if(this.checkFlag(this.typeButtons, powerOf2)) {
            this.typeButtons -= powerOf2;
        } else {
            this.typeButtons += powerOf2;
        }
    }

    updateAtt(att, type) {
        if(att.type != type) {
            --this.stats[att.type];
            ++this.stats[type];

            att.type = type;
            att.enrolled_id = this.enrolled_id;
            this.attendancesService.update(att.class_id, att);

            this.events.publish('studentModified');
        }
    }
}
