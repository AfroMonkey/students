import { Component } from '@angular/core';
import { NavParams, ViewController, ItemSliding, Events,
         AlertController, ModalController } from 'ionic-angular';

import { RubricFormPage } from '../rubric-form/rubric-form';

import { Rubrics } from '../../providers/rubrics-service';

@Component({
    selector: 'page-rubrics',
    templateUrl: 'rubrics.html'
})
export class RubricsPage {
    public section_id;
    public total;
    public modified = false;
    rubrics = [];

    constructor(public navParams: NavParams, public viewCtrl: ViewController,
                public events: Events,
                public modalCtrl: ModalController,
                public alertCtrl: AlertController,
                public rubricsService: Rubrics) {
        this.section_id = this.navParams.get('section_id');
    }

    ionViewDidLoad() {
        this.getAll();
    }

    ionViewWillUnload() {
        if(this.modified) {
            this.events.publish('acts:rubricsUpdated');
            this.events.publish('studs:rubricsUpdated');
        }
    }

    getAll() {
        this.rubricsService.getAll(this.section_id)
            .then(rubrics => {
                this.rubrics = rubrics;
                this.calcTotal();
            })
    }

    calcTotal() {
        this.total = 0;
        for(let r of this.rubrics) {
            this.total += r.percentage;
        }
    }

    add() {
        let modal = this.modalCtrl.create(RubricFormPage,
                                          {section_id: this.section_id});
        modal.onDidDismiss(data => {
            if(data) {
                data.section_id = this.section_id;
                this.rubricsService.add(data)
                    .then(response => {
                        this.getAll();
                        this.modified = true;
                    })
            }
        });
        modal.present();
    }

    modifyRubric(rubric, item: ItemSliding) {
        let modal = this.modalCtrl.create(RubricFormPage,
                                          {oldData: rubric,
                                           section_id: this.section_id});
        modal.onDidDismiss(data => {
            if(data) {
                this.rubricsService.update(rubric.rubric_id, data)
                    .then(response => {
                        this.getAll();
                        this.modified = true;
                    })
            }
        });
        modal.present();
        item.close();
    }

    deleteRubric(rubric, item: ItemSliding) {
        let confirmAlert = this.alertCtrl.create({
            title: '¿Eliminar rúbrica?',
            message: '¿Realmente desea eliminar la rúbrica ' + rubric.name
                     + ' (y todas las actividades asociadas a ella)'
                     + ' de esta sección?',
            buttons: [
                { text: 'Cancelar', handler: () => {} },
                { text: 'Eliminar', handler: () => {
                        this.rubricsService.delete(rubric.rubric_id)
                            .then(response => {
                                this.getAll();
                                this.modified = true;
                            })
                    }
                }
            ]
        });
        confirmAlert.present();
        item.close();
    }

    dismiss() {
        this.viewCtrl.dismiss();
    }
}
