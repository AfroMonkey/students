import { Component, Renderer } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { NavParams, ViewController, ToastController } from 'ionic-angular';

import { Rubrics } from '../../providers/rubrics-service';

@Component({
    selector: 'page-rubric-form',
    templateUrl: 'rubric-form.html'
})
export class RubricFormPage {
    private section_id;
    private data : FormGroup;
    private oldData: any;

    constructor(public navParams: NavParams, public viewCtrl: ViewController,
                public renderer: Renderer,
                private formBuilder: FormBuilder,
                private toastCtrl: ToastController,
                private rubricsService: Rubrics) {
        this.renderer.setElementClass(viewCtrl.pageRef().nativeElement,
                                      'rubric-form-modal', true);
        this.section_id = this.navParams.get('section_id');
        this.oldData = this.navParams.get('oldData');
        if(typeof this.oldData === 'undefined') {
            this.oldData = {name: '', percentage: ''};
        }

        this.data = this.formBuilder.group({
            name:
                [this.oldData.name, Validators.required],
            percentage:
                [this.oldData.percentage,
                 Validators.compose(
                  [Validators.pattern('^100$|^0?[0-9]?[1-9]$|^0?[1-9][0-9]$'),
                   Validators.required])]
        });
    }

    dismiss(data?: any) {
        if(data) {
            this.rubricsService.exists(this.section_id, data.name)
                .then(response => {
                    if(response && (data.name !== this.oldData.name)) {
                        let toast = this.toastCtrl.create({
                            message: '¡La rúbrica ' + data.name + ' ya existe!',
                            duration: 3000,
                            position: 'bottom'
                        });
                        toast.present();
                    } else {
                        this.viewCtrl.dismiss(data);
                    }
                })
        } else {
            this.viewCtrl.dismiss(data);
        }
    }
}
