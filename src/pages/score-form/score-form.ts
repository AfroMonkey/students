import { Component, Renderer } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { NavParams, ViewController } from 'ionic-angular';

@Component({
  selector: 'page-score-form',
  templateUrl: 'score-form.html'
})
export class ScoreFormPage {
    private data : FormGroup;
    private oldData: any;

    constructor(public navParams: NavParams, public viewCtrl: ViewController,
                public renderer: Renderer,
                private formBuilder: FormBuilder) {
        this.renderer.setElementClass(viewCtrl.pageRef().nativeElement,
                                      'score-form-modal', true);
        this.oldData = this.navParams.get('oldData');
        if(typeof this.oldData === 'undefined')
        {
            this.oldData = {score: ''};
        }

        this.data = this.formBuilder.group({
            score:
                [this.oldData.score,
                 Validators.compose([
                     Validators.required,
                     Validators.pattern('^100$|^0?[0-9]?[0-9]$')
                 ])
                ]
        });
    }

    remove() {
        this.viewCtrl.dismiss({remove: true});
    }

    dismiss(data?: any) {
        this.viewCtrl.dismiss(data);
    }
}
