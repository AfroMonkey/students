import { Component, Renderer } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { NavParams, ViewController,
         ModalController, ToastController } from 'ionic-angular';

import { ScheduleFormPage } from '../schedule-form/schedule-form';

import { Sections } from '../../providers/sections-service';

@Component({
  selector: 'page-section-form',
  templateUrl: 'section-form.html'
})
export class SectionFormPage {
    private data : FormGroup;
    private schedule = {};
    private oldData: any;
    private subject_key;

    private minStartDate: string;
    private maxStartDate: string;
    private minEndDate: string;
    private maxEndDate: string;

    private letters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K',
                       'L', 'M', 'N', 'Ñ', 'O', 'P', 'Q', 'R', 'S', 'T', 'U',
                       'V', 'W', 'X', 'Y', 'Z'];
    private schedule_days = ['L', 'M', 'I', 'J', 'V', 'S'];
    private sixDaysInMiliSecs = 518400000;

    constructor(public navParams: NavParams, public viewCtrl: ViewController,
                public renderer: Renderer,
                public modalCtrl: ModalController,
                public toastCtrl: ToastController,
                private formBuilder: FormBuilder,
                public sectionsService: Sections) {
        this.renderer.setElementClass(viewCtrl.pageRef().nativeElement,
                                      'section-form-modal', true);
        this.oldData = this.navParams.get('oldData');
        this.subject_key = this.navParams.get('subject_key');
        if(typeof this.oldData === 'undefined') {
            this.oldData = {code_letter: 'D', start_date: '', end_date: ''};
            this.oldData.calendar_year = new Date().getFullYear().toString();
            this.oldData.calendar_letter = new Date().getMonth() < 5 ? 'A'
                                                                     : 'B';
        } else {
            this.schedule = this.oldData.schedule;
        }

        this.minStartDate = this.oldData.calendar_year + '-01-01';
        this.maxStartDate = this.oldData.calendar_year + '-12-25';
        this.minEndDate = this.oldData.calendar_year + '-01-07';
        this.maxEndDate = this.oldData.calendar_year + '-12-31';

        this.data = this.formBuilder.group({
            calendar_year:
                [this.oldData.calendar_year,
                 Validators.compose([Validators.minLength(4),
                                     Validators.required])],
            calendar_letter:
                [this.oldData.calendar_letter,
                 Validators.required],
            code_letter:
                [this.oldData.code_letter,
                 Validators.required],
            code_number:
                [this.oldData.code_number,
                 Validators.required],
            nrc:
                [this.oldData.nrc,
                 Validators.compose([Validators.pattern('[0-9]{5}'),
                                     Validators.required])],
            start_date:
                [this.oldData.start_date,
                 Validators.required],
            end_date:
                [this.oldData.end_date,
                 Validators.required]
        });
    }

    updateDateLimits() {
        if(this.data.value.calendar_year > 1969) {
            this.minStartDate = this.data.value.calendar_year + '-01-01';
            this.maxStartDate = this.data.value.calendar_year + '-12-25';
            this.minEndDate = this.data.value.calendar_year + '-01-07';
            this.maxEndDate = this.data.value.calendar_year + '-12-31';
            this.data.patchValue({start_date: this.minStartDate});
            this.data.patchValue({end_date: this.maxEndDate});
        }
    }

    setCodePadd() {
        if(this.data.value.code_number.length < 2) {
            this.data.patchValue({code_number:
                                  '0' + this.data.value.code_number});
        }
    }

    date2Str(date): String {
        return date.toISOString().substr(0, 10); // 2017-01-01
    }

    updateEndDate() {
        let start = new Date(this.data.value.start_date);
        if(this.data.value.end_date) {
            let end = new Date(this.data.value.end_date);
            if((end.getTime() - this.sixDaysInMiliSecs) <= start.getTime()) {
                let newEnd = new Date(start.getTime() + this.sixDaysInMiliSecs);
                this.data.patchValue({end_date: this.date2Str(newEnd)});
            }
        } else {
            let newEnd = new Date(start.getTime() + this.sixDaysInMiliSecs);
            this.data.patchValue({end_date: this.date2Str(newEnd)});
        }
    }

    updateStartDate() {
        let end = new Date(this.data.value.end_date);
        if(end < (new Date(this.minEndDate))) {
            this.data.patchValue({end_date: this.minEndDate});
            end = new Date(this.data.value.end_date);
        }
        if(this.data.value.start_date) {
            let start = new Date(this.data.value.start_date);
            if((start.getTime() + this.sixDaysInMiliSecs) >= end.getTime()) {
                let newStart = new Date(end.getTime() - this.sixDaysInMiliSecs);
                this.data.patchValue({start_date: this.date2Str(newStart)});
            }
        } else {
            let newStart = new Date(end.getTime() - this.sixDaysInMiliSecs);
            this.data.patchValue({start_date: this.date2Str(newStart)});
        }
    }

    setSchedule(day) {
        let modal = this.modalCtrl.create(ScheduleFormPage,
                                          {oldData: this.schedule[day]});
        modal.onDidDismiss(data => {
            if(data) {
                if(data.remove) {
                    delete this.schedule[day];
                } else {
                    this.schedule[day] = {start: data.start, end: data.end};
                }
            }
        });
        modal.present();
    }

    scheduleSelected() {
        return Object.keys(this.schedule).length === 0 ? false : true;
    }

    dismiss(data?: any) {
        let code;
        let cal;
        let oldCode = this.oldData.code_letter + this.oldData.code_number;
        let oldCal = this.oldData.calendar_year + this.oldData.calendar_letter;
        if(data) {
            code = data.code_letter + data.code_number;
            cal = data.calendar_year + data.calendar_letter;
        }
        if(data && (data.nrc != this.oldData.nrc || code != oldCode)) {
            Promise.all([
                this.sectionsService.existsNRC(data.nrc),
                this.sectionsService.existsCode(code, cal, this.subject_key)
            ]).then(values => {
                if(values[0] || values[1]) {
                    let toast = this.toastCtrl.create({
                        message: '¡La sección ya existe dentro del' +
                                 ' calendario seleccionado!',
                        duration: 3000,
                        position: 'bottom'
                    });
                    toast.present();
                } else {
                    data.schedule = this.schedule;
                    this.viewCtrl.dismiss(data);
                }
            })
        } else {
            if(data) data.schedule = this.schedule;
            this.viewCtrl.dismiss(data);
        }
    }
}
