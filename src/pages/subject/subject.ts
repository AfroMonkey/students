import { Component } from '@angular/core';
import { NavController, NavParams, ViewController,
         ModalController, AlertController,
         ActionSheetController } from 'ionic-angular';

import { SectionFormPage } from '../section-form/section-form';
import { SectionPage } from '../section/section';

import { Sections } from '../../providers/sections-service';
import { Classes } from '../../providers/classes-service';

import { Formater } from '../../providers/format-service';
import { Utils } from '../../providers/utils-service';

@Component({
  selector: 'page-subject',
  templateUrl: 'subject.html'
})
export class SubjectPage {
    data: any;
    sections: any[];

    constructor(public navCtrl: NavController, public navParams: NavParams,
                public viewCtrl: ViewController,
                public modalCtrl: ModalController,
                public alertCtrl: AlertController,
                public actionShCtrl: ActionSheetController,
                public sectionsService: Sections,
                public classesService: Classes) {
        this.data = navParams.get('subjectData');
    }

    ionViewDidLoad() {
        this.getAll();
    }

    ionViewWillEnter() {
        this.viewCtrl.setBackButtonText('');
    }

    getKeys(schedule) {
        return Utils.getScheduleKeys(schedule);
    }

    getScheduleDays(schedule) {
        let daysOffsets = [];
        if(schedule['L']) daysOffsets.push(1);
        if(schedule['M']) daysOffsets.push(2);
        if(schedule['I']) daysOffsets.push(3);
        if(schedule['J']) daysOffsets.push(4);
        if(schedule['V']) daysOffsets.push(5);
        if(schedule['S']) daysOffsets.push(6);
        return daysOffsets;
    }

    getClassesDay(fromDate, toDate, day /*Monday: 1*/) {
        let dates = [];
        let curr = new Date(fromDate);
        let end = new Date(toDate);

        curr.setUTCDate(curr.getUTCDate() + (day - curr.getUTCDay() + 7) % 7);
        while(curr <= end) {
            dates.push(curr.toISOString().substr(0, 10));
            curr.setUTCDate(curr.getUTCDate() + 7);
        }

        return dates;
    }

    getClasses(fromDate, toDate, daysOffsets) {
        let dates = [];
        for(let day of daysOffsets) {
            dates = dates.concat(this.getClassesDay(fromDate, toDate, day));
        }

        return dates.sort();
    }

    getAll() {
        this.sectionsService.getAll(this.data.subject_key)
            .then(sections => { this.sections = sections; })
    }

    addSection() {
        let modal = this.modalCtrl.create(SectionFormPage,
                                          {subject_key: this.data.subject_key});
        modal.onDidDismiss(data => {
            if(data) {
                data.subject_key = this.data.subject_key;
                // Copies schedule before it becomes a string (sectionsService)
                let copiedSchedule = data.schedule;
                this.sectionsService.add(data)
                    .then(response => {
                        // Generates classes records for the new section
                        let scheduleDays = this.getScheduleDays(copiedSchedule);
                        let dates = this.getClasses(data.start_date,
                                                    data.end_date,
                                                    scheduleDays);
                        this.classesService.setUp(response.insertId,
                                                  copiedSchedule,
                                                  dates)
                            .then(response => { this.getAll(); })
                    })
            }
        });
        modal.present();
    }

    modifySection(section) {
        let oldSchStr = Formater.schedule2Str(section.schedule);
        let modal = this.modalCtrl.create(SectionFormPage,
                                          {oldData: section,
                                           subject_key: this.data.subject_key});
        modal.onDidDismiss(data => {
            if(data) {
                // Copies schedule before it becomes a string (sectionsService)
                let copiedSchedule = data.schedule;
                this.sectionsService.update(section.section_id, data)
                .then(response => {
                    // Checks if we need re-setup the classes
                    if(oldSchStr != data.schedule ||
                       section.start_date != data.start_date ||
                       section.end_date != data.end_date) {
                        let scheduleDays = this.getScheduleDays(copiedSchedule);
                        let dates = this.getClasses(data.start_date,
                                                    data.end_date,
                                                    scheduleDays);
                        this.classesService.resetClasses(section.section_id,
                                                         copiedSchedule, dates);
                    }
                    this.getAll();
                })
            }
        });
        modal.present();
    }

    deleteSection(section) {
        let confirmAlert = this.alertCtrl.create({
            title: '¿Eliminar sección?',
            message: '¿Realmente desea eliminar la sección ' +
                      section.code + ` y todos los datos asociados
                      a ella?`,
            buttons: [
                { text: 'Cancelar', handler: () => {} },
                { text: 'Eliminar', handler: () => {
                    this.sectionsService.delete(section.section_id).then(
                        response => { this.getAll(); }
                    )}
                }
            ]
        });
        confirmAlert.present();
    }

    openSection(section) {
        this.navCtrl.push(SectionPage, {sectionData : section});
    }

    actionsSection(section) {
        let actionSheet = this.actionShCtrl.create({
            title: section.subject_key + '-' + section.nrc,
            buttons: [
                { text: 'Eliminar', role: 'destructive',
                  handler: () => { this.deleteSection(section); } },
                { text: 'Modificar',
                  handler: () => { this.modifySection(section); } },
                { text: 'Cancelar', role: 'cancel' }
            ]
        });
        actionSheet.present();
    }
}
