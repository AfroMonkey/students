import { Injectable } from '@angular/core';
import { Database } from '../providers/database-service';

@Injectable()
export class Deliveries {
    constructor(public db: Database) {}

    add(activity_id, dev) {
        let sqlStmt = `INSERT INTO deliveries(enrolled_id, activity_id, score)
                       VALUES(?, ?, ?)`;
        return this.db.query(sqlStmt, [dev.enrolled_id, activity_id,
                                       dev.score]);
    }

    update(activity_id, dev) {
        let sqlStmt = `UPDATE deliveries
                       SET score = ?
                       WHERE enrolled_id = ? AND activity_id = ?`;
        return this.db.query(sqlStmt, [dev.score, dev.enrolled_id,
                                       activity_id]);
    }

    delete(activity_id, enrolled_id) {
        let sqlStmt = `DELETE FROM deliveries
                       WHERE activity_id = ? AND enrolled_id = ?`;
        return this.db.query(sqlStmt, [activity_id, enrolled_id]);
    }

    getAll(section_id, activity_id) {
        let sqlStmt = `SELECT
                           enrolled_students.enrolled_id, student_code,
                           name, score
                       FROM enrolled_students
                       NATURAL JOIN students
                       LEFT JOIN
                           (SELECT * FROM deliveries
                            WHERE activity_id = ?) AS delivs
                           ON enrolled_students.enrolled_id = delivs.enrolled_id
                       WHERE
                           section_id = ?`;
        return this.db.query(sqlStmt, [activity_id, section_id])
            .then(response => {
                let deliveries = [];
                for(let index = 0; index < response.rows.length; ++index) {
                    deliveries.push(response.rows.item(index));
                }
                return Promise.resolve(deliveries);
            })
    }

    getAllByEnrolled(enrolled_id) {
        let sqlStmt = `SELECT
                         activities.activity_id,
                         type,
                         activities.name,
                         score,
                         score NOT NULL AS delivered
                       FROM
	                     activities
                       JOIN
	                     rubrics
                       ON
	                     activities.rubric_id = rubrics.rubric_id
                       NATURAL JOIN
	                     enrolled_students
                       LEFT JOIN
	                     deliveries
                       ON
	                     activities.activity_id = deliveries.activity_id AND
	                     enrolled_students.enrolled_id = deliveries.enrolled_id
                       WHERE
	                     enrolled_students.enrolled_id = ?`;
        return this.db.query(sqlStmt, [enrolled_id])
            .then(response => {
                let acts = [];
                for(let index = 0; index < response.rows.length; ++index) {
                    acts.push(response.rows.item(index));
                }
                return Promise.resolve(acts);
            })
    }
}
