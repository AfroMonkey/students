import { Injectable } from '@angular/core';
import { SQLite } from 'ionic-native';

import { AlertController } from 'ionic-angular';

@Injectable()
export class Database {
    db: SQLite = null;

    constructor(public alertCtrl : AlertController) {
        this.db = new SQLite();
    }

    open() {
        return this.db.openDatabase({ name: 'data.db', location: 'default'})
            .then(success => {
                    return this.db.executeSql('PRAGMA foreign_keys = ON', [])
            });
    }

    setupScheme() {
        return Promise.all([
            this.createSubjectsTable(),
            this.createSectionsTable(),
            this.createStudentsTable(),
            this.createEnrolledStudsTable(),
            this.createClassesTable(),
            this.createAttendancesTable(),
            this.createRubricsTable(),
            this.createActivitiesTable(),
            this.createDeliveriesTable(),
            this.createExtraordinaryTable()
        ])
    }

    query(statement, args) {
        return this.db.executeSql(statement, args);
    }

    createSubjectsTable() {
        let subjectsStmt = `CREATE TABLE IF NOT EXISTS
                            subjects(
                                subject_key TEXT PRIMARY KEY,
                                name TEXT
                            )`;
        return this.db.executeSql(subjectsStmt, []);
    }

    createSectionsTable() {
        let sectionsStmt = `CREATE TABLE IF NOT EXISTS
                            sections(
                                section_id INTEGER PRIMARY KEY,
                                subject_key TEXT,
                                nrc INTEGER,
                                calendar TEXT,
                                code TEXT,
                                schedule TEXT,
                                start_date TEXT,
                                end_date TEXT,
                                FOREIGN KEY(subject_key)
                                    REFERENCES subjects(subject_key)
                                    ON UPDATE CASCADE
                                    ON DELETE CASCADE
                            )`;
        return this.db.executeSql(sectionsStmt, []);
    }

    createStudentsTable() {
        let studentsStmt = `CREATE TABLE IF NOT EXISTS
                            students(
                                student_code INTEGER PRIMARY KEY,
                                name TEXT
                            )`;
        return this.db.executeSql(studentsStmt, []);
    }

    createEnrolledStudsTable() {
        let sectStudStmt = `CREATE TABLE IF NOT EXISTS
                            enrolled_students(
                                enrolled_id INTEGER PRIMARY KEY,
                                student_code INTEGER,
                                section_id INTEGER,
                                FOREIGN KEY(student_code)
                                    REFERENCES students(student_code)
                                    ON UPDATE CASCADE
                                    ON DELETE CASCADE,
                                FOREIGN KEY(section_id)
                                    REFERENCES sections(section_id)
                                    ON DELETE CASCADE
                            )`;
        return this.db.executeSql(sectStudStmt, []);
    }

    createClassesTable() {
        let classesStmt = `CREATE TABLE IF NOT EXISTS
                           classes(
                               class_id INTEGER PRIMARY KEY,
                               section_id INTEGER,
                               num_sessions INTEGER,
                               type TEXT,
                               date TEXT,
                               topic TEXT,
                               instrument TEXT,
                               method TEXT,
                               FOREIGN KEY(section_id)
                                   REFERENCES sections(section_id)
                                   ON DELETE CASCADE
                           )`;
        return this.db.executeSql(classesStmt, []);
    }

    createAttendancesTable() {
        let studClassStmt = `CREATE TABLE IF NOT EXISTS
                             attendances(
                                 enrolled_id INTEGER,
                                 class_id INTEGER,
                                 type INTEGER,
                                 PRIMARY KEY(enrolled_id, class_id),
                                 FOREIGN KEY(enrolled_id)
                                     REFERENCES enrolled_students(enrolled_id)
                                     ON UPDATE CASCADE
                                     ON DELETE CASCADE,
                                 FOREIGN KEY(class_id)
                                     REFERENCES classes(class_id)
                                     ON DELETE CASCADE
                             )`;
        return this.db.executeSql(studClassStmt, []);
    }

    createRubricsTable() {
        let rubricsStmt = `CREATE TABLE IF NOT EXISTS
                           rubrics(
                               rubric_id INTEGER PRIMARY KEY,
                               section_id INTEGER,
                               name TEXT,
                               percentage INTEGER,
                               FOREIGN KEY(section_id)
                                   REFERENCES sections(section_id)
                                   ON DELETE CASCADE
                           )`;
        return this.db.executeSql(rubricsStmt, []);
    }

    createActivitiesTable() {
        let activitiesStmt = `CREATE TABLE IF NOT EXISTS
                              activities(
                                  activity_id INTEGER PRIMARY KEY,
                                  rubric_id INTEGER,
                                  type INTEGER,
                                  name TEXT,
                                  topic TEXT,
                                  due_date TEXT,
                                  FOREIGN KEY(rubric_id)
                                      REFERENCES rubrics(rubric_id)
                                      ON DELETE CASCADE
                              )`;
        return this.db.executeSql(activitiesStmt, []);
    }

    createDeliveriesTable() {
        let studActStmt = `CREATE TABLE IF NOT EXISTS
                           deliveries(
                               enrolled_id INTEGER,
                               activity_id INTEGER,
                               score INTEGER,
                               PRIMARY KEY(enrolled_id, activity_id),
                               FOREIGN KEY(enrolled_id)
                                   REFERENCES enrolled_students(enrolled_id)
                                   ON UPDATE CASCADE
                                   ON DELETE CASCADE,
                               FOREIGN KEY(activity_id)
                                   REFERENCES activities(activity_id)
                                   ON DELETE CASCADE
                           )`;
        return this.db.executeSql(studActStmt, []);
    }

    createExtraordinaryTable() {
        let extStmt = `CREATE TABLE IF NOT EXISTS
                       extraordinary(
                           enrolled_id INTEGER,
                           ext_score INTEGER,
                           PRIMARY KEY(enrolled_id),
                           FOREIGN KEY(enrolled_id)
                               REFERENCES enrolled_students(enrolled_id)
                               ON DELETE CASCADE
                       )`;
        return this.db.executeSql(extStmt, []);
    }
}
