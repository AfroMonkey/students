import { Injectable } from '@angular/core';

@Injectable()
export class Formater {
    static monthNames = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
                         'Julio', 'Agosto', 'Septiembre', 'Octubre',
                         'Noviembre', 'Diciembre'];

    constructor() {}

    static formatDate(dateISOStr) {
        return dateISOStr.substr(8, 2) + ' ' +
               this.monthNames[Number(dateISOStr.substr(5, 2)) - 1];
    }

    static schedule2Str(schedule) {
        let str: String = '';
        for(let item in schedule) {
            str += item;                               // L
            str += schedule[item].start.substr(0, 2);  // 07
            str += schedule[item].end.substr(0, 2);    // 09
        }
        return str;
    }
}
