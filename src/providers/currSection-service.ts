import { Injectable } from '@angular/core';

@Injectable()
export class CurrSection {
    section_data: any;
    active: boolean;

    constructor() {}

    setActive(isActive: boolean) {
        this.active = isActive;
    }

    setData(data: any) {
        this.section_data = data;
    }

    isActive() {
        return this.active;
    }

    data() {
        return this.section_data;
    }
}
