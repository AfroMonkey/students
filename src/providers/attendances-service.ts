import { Injectable } from '@angular/core';
import { Database } from '../providers/database-service';

@Injectable()
export class Attendances {
    constructor(public db: Database) {}

    add(class_id, att) {
        let sqlStmt = `INSERT INTO
                           attendances(enrolled_id, class_id, type)
                       VALUES(?, ?, ?)`;
        return this.db.query(sqlStmt, [att.enrolled_id, class_id, att.type]);
    }

    update(class_id, att) {
        let sqlStmt = `UPDATE attendances
                       SET type = ?
                       WHERE enrolled_id = ? AND class_id = ?`;
        return this.db.query(sqlStmt, [att.type, att.enrolled_id, class_id]);
    }

    getAll(section_id, class_id) {
        let sqlStmt = `SELECT
                           enrolled_students.enrolled_id, student_code,
                           name, type
                       FROM enrolled_students
                       NATURAL JOIN students
                       LEFT JOIN
                           (SELECT * FROM attendances
                            WHERE class_id = ?) AS atts
                           ON enrolled_students.enrolled_id = atts.enrolled_id
                       WHERE
                           section_id = ?`;
        return this.db.query(sqlStmt, [class_id, section_id])
            .then(response => {
                let attendances = [];
                for(let index = 0; index < response.rows.length; ++index) {
                    attendances.push(response.rows.item(index));
                }
                return Promise.resolve(attendances);
            })
    }

    getAllByEnrolled(enrolled_id) {
        let sqlStmt = `SELECT
                           classes.class_id,
                           num_sessions,
                           date,
                           attendances.type
                       FROM
                           attendances
                       JOIN
                           classes
                       ON
                           attendances.class_id = classes.class_id
                       WHERE
                           enrolled_id = ?`;
        return this.db.query(sqlStmt, [enrolled_id])
            .then(response => {
                let attendances = [];
                for(let index = 0; index < response.rows.length; ++index) {
                    attendances.push(response.rows.item(index));
                }
                return Promise.resolve(attendances);
            })
    }

    getStatsByEnrolled(enrolled_id) {
        let sqlStmt = `SELECT
                           COUNT(num_sessions) AS total,
                           COUNT(CASE WHEN attendances.type = 'A' THEN
                                     num_sessions
                                 END) AS A,
                           COUNT(CASE WHEN attendances.type = 'R' THEN
                                     num_sessions
                                 END) AS R,
                           COUNT(CASE WHEN attendances.type = 'F' THEN
                                     num_sessions
                                 END) AS F
                       FROM
                           attendances
                       JOIN
                           classes
                       ON
                           attendances.class_id = classes.class_id
                       WHERE
                           enrolled_id = ?`;
            return this.db.query(sqlStmt, [enrolled_id])
                .then(response => {
                    let result = response.rows.length ? response.rows.item(0)
                                                      : {total: 0,
                                                         A: 0,
                                                         R: 0,
                                                         F: 0};
                    return Promise.resolve(result);
                })
    }
}
