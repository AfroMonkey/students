import { Injectable } from '@angular/core';
import { Database } from '../providers/database-service';

@Injectable()
export class Extraordinary {
    constructor(public db: Database) {}

    add(enrolled_id, score) {
        let sqlStmt = `INSERT INTO extraordinary(enrolled_id, ext_score)
                       VALUES(?, ?)`;
        return this.db.query(sqlStmt, [enrolled_id, score]);
    }

    update(enrolled_id, score) {
        let sqlStmt = `UPDATE extraordinary
                       SET ext_score = ?
                       WHERE enrolled_id = ?`;
        return this.db.query(sqlStmt, [score, enrolled_id]);
    }

    delete(enrolled_id) {
        let sqlStmt = 'DELETE FROM extraordinary WHERE enrolled_id = ?';
        return this.db.query(sqlStmt, [enrolled_id]);
    }
}
