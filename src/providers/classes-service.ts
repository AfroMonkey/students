import { Injectable } from '@angular/core';
import { Database } from '../providers/database-service';

@Injectable()
export class Classes {
    constructor(public db: Database) {}

    private insertDatesIntoTable(section_id, schedule, dates, tableName) {
        let results = [];
        let days = ['D', 'L', 'M', 'I', 'J', 'V', 'S'];
        let currSchedule, start, end;
        for(let date of dates) {
            currSchedule = schedule[days[new Date(date).getUTCDay()]];
            start = Number(currSchedule.start.substr(0, 2));
            end = Number(currSchedule.end.substr(0, 2));
            let sqlStmt = 'INSERT INTO ' + tableName +
                          '(section_id, num_sessions, date) ' +
                          'VALUES(?, ?, ?)';
            this.db.query(sqlStmt, [section_id,
                                    (end - start) > 2 ? 2 : 1,
                                    date])
                .then(response => { results.push(response); })
        }
        return Promise.resolve(results);
    }

    private updateNumSessions(class_id, new_num_sessions) {
        let sqlStmt = 'UPDATE classes SET num_sessions = ? WHERE class_id = ?';
        return this.db.query(sqlStmt, [new_num_sessions, class_id])
        .then(res => {
            if(new_num_sessions == 1) {
                return this.tardee2Absent(class_id);
            }
        })
    }

    private tardee2Absent(class_id) {
        let sqlType = `UPDATE attendances SET type = 'F'
                       WHERE class_id = ? AND type = 'R'`
        return this.db.query(sqlType, [class_id]);
    }

    setUp(section_id, schedule, dates) {
        return this.insertDatesIntoTable(section_id, schedule,
                                         dates, 'classes');
    }

    resetClasses(section_id, new_schedule, new_dates) {
        let sqlTmp = `CREATE TABLE
                      tmpClasses(
                          section_id INTEGER,
                          num_sessions INTEGER,
                          date TEXT
                      )`;
        let sqlDrop = 'DROP TABLE tmpClasses';
        let sqlAddNew = `INSERT INTO classes(section_id, num_sessions, date)
                         SELECT * FROM tmpClasses
                         WHERE date NOT IN
                         ( SELECT date FROM classes WHERE section_id = ? )`;
        let sqlDelOld = `DELETE FROM classes
                         WHERE section_id = ? AND date NOT IN
                         ( SELECT date FROM tmpClasses )`;
        let sqlUpdate = `SELECT
                             class_id,
                             classes.section_id,
                             classes.date,
                             classes.num_sessions AS old_num_sessions,
                             tmpClasses.num_sessions AS new_num_sessions
                         FROM classes
                         JOIN tmpClasses
                             ON classes.date = tmpClasses.date
                         WHERE
                             classes.section_id = ? AND
                             old_num_sessions != new_num_sessions`;
        // Puts new_dates in temp table
        return this.db.query(sqlTmp, []).then(tmpCreated => {
            return this.insertDatesIntoTable(section_id, new_schedule,
                                             new_dates, 'tmpClasses')
            // Inserts new dates
            .then(tmpInserted => {
                return this.db.query(sqlAddNew, [section_id]);
            })
            // Deletes old dates
            .then(newInserted => {
                return this.db.query(sqlDelOld, [section_id]);
            })
            // Updates untouched dates
            .then(oldDeleted => {
                return this.db.query(sqlUpdate, [section_id]).then(res => {
                    let results = [];
                    for(let index = 0; index < res.rows.length; ++index) {
                        let curr = res.rows.item(index);
                        this.updateNumSessions(curr.class_id,
                                               curr.new_num_sessions)
                        .then(numSessionsUpdated => {
                            results.push(numSessionsUpdated);
                        })
                    }
                    return Promise.resolve(results);
                })
            })
            // Drops temp table
            .then(finished => {
                return this.db.query(sqlDrop, []);
            })
        })
    }

    setData(class_id, data) {
        let sqlStmt = `UPDATE classes
                       SET type = ?, topic = ?, instrument = ?, method = ?
                       WHERE class_id = ?`;
        return this.db.query(sqlStmt, [data.type, data.topic, data.instrument,
                                       data.method, class_id]);
    }

    getActive(section_id) {
        let sqlStmt = `SELECT date, num_sessions
                       FROM classes
                       WHERE section_id = ? AND type = 'N'
                       ORDER BY date ASC`;
        return this.db.query(sqlStmt, [section_id])
            .then(response => {
                let classes = [];
                for(let index = 0; index < response.rows.length; ++index) {
                    classes.push(response.rows.item(index));
                }
                return Promise.resolve(classes);
            })
    }

    getNumOfAtts(section_id, date) {
        let sqlStmt = `SELECT
	                       ifnull(
                            SUM(CASE WHEN attendances.type = 'A' THEN
                                    num_sessions
                                END) +
                            COUNT(CASE WHEN attendances.type = 'R' THEN
                                      num_sessions
                                  END)
                           , 0) AS total
                       FROM classes
                       JOIN attendances
                           ON classes.class_id = attendances.class_id
                       WHERE section_id = ? AND date = ?`;
        return this.db.query(sqlStmt, [section_id, date])
            .then(response => {
                return Promise.resolve(response.rows.item(0).total);
            })
    }

    getAll(section_id) {
        let sqlStmt = `SELECT * FROM classes
                       WHERE section_id = ?
                       ORDER BY date ASC`;
        return this.db.query(sqlStmt, [section_id])
            .then(response => {
                let classes = [];
                for(let index = 0; index < response.rows.length; ++index) {
                    classes.push(response.rows.item(index));
                }
                return Promise.resolve(classes);
            })
    }
}
