import { Injectable } from '@angular/core';
import { Database } from '../providers/database-service';

@Injectable()
export class Rubrics {
    constructor(public db: Database) {}

    add(rubric) {
        let sqlStmt = `INSERT INTO rubrics(section_id, name, percentage)
                       VALUES(?, ?, ?)`;
        return this.db.query(sqlStmt, [rubric.section_id, rubric.name,
                                       rubric.percentage]);
    }

    update(rubric_id, rubric) {
        let sqlStmt = `UPDATE rubrics
                       SET name = ?, percentage = ?
                       WHERE rubric_id = ?`;
        return this.db.query(sqlStmt, [rubric.name, rubric.percentage,
                                       rubric_id]);
    }

    delete(rubric_id) {
        let sqlStmt = 'DELETE FROM rubrics WHERE rubric_id = ?';
        return this.db.query(sqlStmt, [rubric_id]);
    }

    exists(section_id, rubric_name) {
        let sqlStmt = `SELECT * FROM rubrics
                       WHERE section_id = ? AND name = ?
                       LIMIT 1`;
        return this.db.query(sqlStmt, [section_id, rubric_name])
            .then(response => {
                return Promise.resolve(response.rows.length);
            })
    }

    numberOfActs(rubric_id, type) {
        let sqlStmt = `SELECT * FROM activities
                       WHERE rubric_id = ? AND type = ?`;
        return this.db.query(sqlStmt, [rubric_id, type])
            .then(response => {
                return Promise.resolve(response.rows.length);
            })
    }

    getAll(section_id) {
        let sqlStmt = 'SELECT * FROM rubrics WHERE section_id = ?';
        return this.db.query(sqlStmt, [section_id])
            .then(response => {
                let rubrics = [];
                for(let index = 0; index < response.rows.length; ++index) {
                    rubrics.push(response.rows.item(index));
                }
                return Promise.resolve(rubrics);
            })
    }
}
