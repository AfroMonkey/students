import { Injectable } from '@angular/core';
import { Database } from '../providers/database-service';

@Injectable()
export class Students {
    constructor(public db: Database) {}

    private create(student) {
        let sqlStmt = 'INSERT INTO students(student_code, name) VALUES(?, ?)';
        return this.db.query(sqlStmt, [student.student_code, student.name]);
    }

    private enroll(student_code, section_id) {
        let sqlStmt = `INSERT INTO enrolled_students(student_code, section_id)
                       VALUES(?, ?)`;
        return this.db.query(sqlStmt, [student_code, section_id]);
    }

    private updateName(student) {
        let sqlStmt = 'UPDATE students SET name = ? WHERE student_code = ?';
        return this.db.query(sqlStmt, [student.name, student.student_code]);
    }

    private updateEnrolled(enrolled_id, old_code, new_code) {
        let sqlStmt = `UPDATE enrolled_students
                       SET student_code = ?
                       WHERE enrolled_id = ?`;
        return this.db.query(sqlStmt, [new_code, enrolled_id])
        .then(() => {
            return this.tryClean(old_code);
        })
    }

    private deleteStudent(student_code) {
        let sqlStmt = 'DELETE FROM students WHERE student_code = ?';
        return this.db.query(sqlStmt, [student_code]);
    }

    private tryClean(old_code) {
        let sqlStmt = `SELECT * FROM enrolled_students
                       WHERE student_code = ?
                       LIMIT 1`;
        return this.db.query(sqlStmt, [old_code]).then(response => {
            if(!response.rows.length) {
                return this.deleteStudent(old_code);
            }
        })
    }

    add(student, section_id, exists) {
        if(exists) {
            return this.enroll(student.student_code, section_id);
        } else {
            return this.create(student)
                .then(() => {
                    return this.enroll(student.student_code, section_id);
                })
        }
    }

    update(old_data, new_data) {
        if(old_data.student_code == new_data.student_code) {
            return this.updateName(new_data);
        } else {
            if(new_data.exists) {
                return this.updateEnrolled(old_data.enrolled_id,
                                           old_data.student_code,
                                           new_data.student_code)
            } else {
                return this.create(new_data)
                .then(() => {
                    return this.updateEnrolled(old_data.enrolled_id,
                                               old_data.student_code,
                                               new_data.student_code)
                })
            }
        }
    }

    delete(student) {
        let sqlStmt = 'DELETE FROM enrolled_students WHERE enrolled_id = ?';
        return this.db.query(sqlStmt, [student.enrolled_id])
        .then(() => {
            return this.tryClean(student.student_code);
        })
    }

    name(student_code) {
        let sqlStmt = 'SELECT * FROM students WHERE student_code = ? LIMIT 1';
        return this.db.query(sqlStmt, [student_code])
            .then(response => {
                if(response.rows.length) {
                    return Promise.resolve(response.rows.item(0).name);
                } else {
                    return Promise.resolve('');
                }
            })
    }

    isEnrolled(student_code, section_id) {
        let sqlStmt = `SELECT * FROM enrolled_students
                       WHERE student_code = ? AND section_id = ?
                       LIMIT 1`;
        return this.db.query(sqlStmt, [student_code, section_id])
            .then(response => {
                return Promise.resolve(response.rows.length);
            })
    }

    getAttendances(enrolled_id) {
        let sqlStmt = `SELECT attendances.type
                       FROM attendances
                       JOIN classes
                         ON attendances.class_id = classes.class_id
                       WHERE enrolled_id = ?
                       ORDER BY date ASC`;
        return this.db.query(sqlStmt, [enrolled_id])
            .then(response => {
                let atts = [];
                for(let index = 0; index < response.rows.length; ++index) {
                    atts.push(response.rows.item(index));
                }
                return Promise.resolve(atts);
            })
    }

    getDeliveries(enrolled_id) {
        let sqlStmt = `SELECT
	                       name, score
                       FROM deliveries
                       NATURAL JOIN activities
                       WHERE enrolled_id = ?
                       ORDER BY due_date ASC`
        return this.db.query(sqlStmt, [enrolled_id])
            .then(response => {
                let delivs = [];
                for(let index = 0; index < response.rows.length; ++index) {
                    delivs.push(response.rows.item(index));
                }
                return Promise.resolve(delivs);
            })
    }

    getAll(section_id) {
        let stmt = `SELECT
                      enrolled_id,
                      student_code,
                      name,
                      ext_score,
                      ifnull(SUM(total_rubric), 0) AS ord_grade,
                      CASE WHEN ext_score IS NULL THEN
                        ifnull(SUM(total_rubric), 0)
                      ELSE
                        ifnull(SUM(total_rubric), 0) * 0.4 + ext_score * 0.8
                      END AS grade,
                      ifnull(total_atts / (total_sessions * 1.0) * 100, 0)
                        AS att_percent
                    FROM (
                      SELECT
                        enrolled_id,
                        student_code,
                        name,
                        rubric_id,
                        ifnull(percentage / rubric_norm_acts / 100.0 *
                               rubric_score, 0) +
                          rubric_extra_pts AS total_rubric,
                        ext_score
                      FROM (
                        SELECT
                          enrolled_students.enrolled_id AS enrolled_id,
                          student_code,
                          name,
                          rubric_id,
                          percentage,
                          COUNT(CASE WHEN type = 0 THEN
                                  activities.activity_id
                                END) AS rubric_norm_acts,
                          SUM(CASE WHEN type < 2 THEN
                                score
                              ELSE
                                0
                              END) AS rubric_score,
                          SUM(CASE WHEN type = 2 THEN
                                score
                              ELSE
                                0
                              END) AS rubric_extra_pts,
                          ext_score
                        FROM
                          students
                        NATURAL JOIN
                          enrolled_students
                        LEFT JOIN (
                          SELECT
                            activities.activity_id,
                            rubrics.rubric_id,
                            percentage,
                            type
                          FROM
                            activities
                          JOIN
                            rubrics
                          ON
                            activities.rubric_id = rubrics.rubric_id
                          WHERE rubrics.section_id = ?
                        ) AS activities
                        ON
                          1 = 1
                        LEFT JOIN
                          deliveries
                        ON
                          enrolled_students.enrolled_id = deliveries.enrolled_id
                          AND
                          activities.activity_id = deliveries.activity_id
                        LEFT JOIN
                          extraordinary
                        ON
                          enrolled_students.enrolled_id =
                          extraordinary.enrolled_id
                        WHERE enrolled_students.section_id = ?
                        GROUP BY enrolled_students.enrolled_id, rubric_id
                      ) AS students_rubrics_summary
                    ) AS students_grades
                    NATURAL JOIN (
                      SELECT
                        enrolled_students.enrolled_id AS enrolled_id,
                        total_sessions,
                        ifnull(
                         SUM(CASE WHEN attendances.type = 'A' THEN
                               num_sessions
                             END) +
                         COUNT(CASE WHEN attendances.type = 'R' THEN
                                 num_sessions
                               END)
                        , 0) AS total_atts
                      FROM
                        enrolled_students
                      LEFT JOIN
                        attendances
                      ON
                        enrolled_students.enrolled_id = attendances.enrolled_id
                      LEFT JOIN (
                        SELECT class_id, num_sessions FROM classes
                      ) AS classes_num_sessions
                      ON
                        attendances.class_id = classes_num_sessions.class_id
                      CROSS JOIN (
                        SELECT
                          ifnull(SUM(num_sessions), 0) AS total_sessions
                        FROM
                          classes
                        WHERE
                          section_id = ? AND type = 'N'
                      ) AS section_total_sessions
                      WHERE
                        enrolled_students.section_id = ?
                      GROUP BY
                        enrolled_students.enrolled_id
                    ) AS students_atts
                    GROUP BY enrolled_id`;
        return this.db.query(stmt, [section_id, section_id,
                                    section_id, section_id])
            .then(response => {
                let students = [];
                for(let index = 0; index < response.rows.length; ++index) {
                    students.push(response.rows.item(index));
                }
                return Promise.resolve(students);
            })
    }
}
