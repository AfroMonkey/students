import { Injectable } from '@angular/core';

@Injectable()
export class Utils {
    static getScheduleKeys(schedule) {
        let keys = [];
        if(schedule['L']) keys.push('L');
        if(schedule['M']) keys.push('M');
        if(schedule['I']) keys.push('I');
        if(schedule['J']) keys.push('J');
        if(schedule['V']) keys.push('V');
        if(schedule['S']) keys.push('S');
        return keys;
    }
}
