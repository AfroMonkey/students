import { Injectable } from '@angular/core';
import { Database } from '../providers/database-service';

import { Rubrics } from '../providers/rubrics-service';

@Injectable()
export class Activities {
    constructor(public db: Database, public rubricsService: Rubrics) {}

    add(act) {
        let sqlStmt = `INSERT INTO
                           activities(rubric_id, type, name, topic, due_date)
                       VALUES(?, ?, ?, ?, ?)`;
        return this.db.query(sqlStmt, [act.rubric_id, act.type, act.name,
                                       act.topic, act.due_date]);
    }

    update(activity_id, act) {
        let sqlStmt = `UPDATE activities
                       SET rubric_id = ?, type = ?, name = ?, topic = ?,
                           due_date = ?
                       WHERE activity_id = ?`;
        return this.db.query(sqlStmt, [act.rubric_id, act.type, act.name,
                                       act.topic, act.due_date, activity_id]);
    }

    delete(activity_id) {
        let sqlStmt = 'DELETE FROM activities WHERE activity_id = ?';
        return this.db.query(sqlStmt, [activity_id]);
    }

    exists(section_id, activity_name) {
        let sqlStmt = `SELECT section_id, activities.name
                       FROM
                           activities JOIN rubrics
                               ON activities.rubric_id = rubrics.rubric_id
                       WHERE section_id = ? AND activities.name = ?
                       LIMIT 1`;
        return this.db.query(sqlStmt, [section_id, activity_name])
            .then(response => {
                return Promise.resolve(response.rows.length);
            })
    }

    canChangeRubric(activity) {
        if(activity.type != 0) return Promise.resolve(true);
        return this.rubricsService.numberOfActs(activity.rubric_id, 1)
        .then(numOfExtraActs => {
            if(numOfExtraActs) {
                return this.rubricsService.numberOfActs(activity.rubric_id, 0)
                .then(numOfNormActs => {
                    return Promise.resolve(numOfNormActs > 1);
                })
            } else {
                return Promise.resolve(true);
            }
        })
    }

    getAll(section_id) {
        let sqlStmt = `SELECT
                           activity_id, type, activities.rubric_id,
                           activities.name, topic, due_date
                       FROM
                           activities JOIN rubrics
                               ON activities.rubric_id = rubrics.rubric_id
                       WHERE
                           section_id = ?`;
        return this.db.query(sqlStmt, [section_id])
            .then(response => {
                let activities = [];
                for(let index = 0; index < response.rows.length; ++index) {
                    activities.push(response.rows.item(index));
                }
                return Promise.resolve(activities);
            })
    }

    getByTopic(section_id, topic) {
        let sqlStmt = `SELECT
                           activities.name
                       FROM
                           activities JOIN rubrics
                               ON activities.rubric_id = rubrics.rubric_id
                       WHERE
                           section_id = ? AND topic = ?`;
        return this.db.query(sqlStmt, [section_id, topic])
            .then(response => {
                let activities = [];
                for(let index = 0; index < response.rows.length; ++index) {
                    activities.push(response.rows.item(index).name);
                }
                return Promise.resolve(activities);
            })
    }
}
