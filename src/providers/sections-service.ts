import { Injectable } from '@angular/core';
import { Database } from '../providers/database-service';

import { Formater } from '../providers/format-service';

@Injectable()
export class Sections {
    constructor(public db: Database) {}

    prepareData(section) {
        section.calendar = section.calendar_year + section.calendar_letter;
        section.code = section.code_letter + section.code_number;
        section.schedule = Formater.schedule2Str(section.schedule);
        delete section['calendar_year'];
        delete section['calendar_letter'];
        delete section['code_letter'];
        delete section['code_number'];
    }

    add(section) {
        let sqlStmt = `INSERT INTO sections(subject_key, nrc, calendar, code,
                                            schedule, start_date, end_date)
                       VALUES(?, ?, ?, ?, ?, ?, ?)`;
        this.prepareData(section);
        return this.db.query(sqlStmt, [section.subject_key, section.nrc,
                                       section.calendar, section.code,
                                       section.schedule, section.start_date,
                                       section.end_date]);
    }

    update(section_id, section) {
        let sqlStmt = `UPDATE sections
                       SET nrc = ?, calendar = ?, code = ?, schedule = ?,
                           start_date = ?, end_date = ?
                       WHERE section_id = ?`;
        this.prepareData(section);
        return this.db.query(sqlStmt, [section.nrc, section.calendar,
                                       section.code, section.schedule,
                                       section.start_date, section.end_date,
                                       section_id]);
    }

    delete(section_id) {
        let sqlStmt = 'DELETE FROM sections WHERE section_id = ?';
        return this.db.query(sqlStmt, [section_id]);
    }

    existsNRC(nrc) {
        let sqlStmt = `SELECT * FROM sections
                       WHERE nrc = ?
                       LIMIT 1`;
        return this.db.query(sqlStmt, [nrc])
            .then(response => {
                return Promise.resolve(response.rows.length);
            })
    }

    existsCode(code, calendar, subject_key) {
        let sqlStmt = `SELECT * FROM sections
                       WHERE code = ? AND calendar = ? AND subject_key = ?
                       LIMIT 1`;
        return this.db.query(sqlStmt, [code, calendar, subject_key])
            .then(response => {
                return Promise.resolve(response.rows.length);
            })
    }

    getAll(subject_key) {
        let sqlStmt = `SELECT * FROM sections
                       WHERE subject_key = ?
                       ORDER BY code ASC`;
        return this.db.query(sqlStmt, [subject_key])
            .then(response => {
                let sections = [];
                for(let index = 0; index < response.rows.length; ++index) {
                    let section = response.rows.item(index);
                    // Divides code into code_letter and code_number
                    section.code_letter = section.code.substr(0, 1);
                    section.code_number = section.code.substr(1, 2);
                    delete section['code'];
                    // Divides calendar into calendar_year and calendar_letter
                    section.calendar_year = section.calendar.substr(0, 4);
                    section.calendar_letter = section.calendar.substr(4, 1);
                    delete section['calendar'];
                    // Parses schedule
                    let schedule = section.schedule;
                    let parsed_schedule = {};
                    for(let i = 0; i < schedule.length; i += 5) {
                        let start_time = schedule.substr(i + 1, 2) + ':00';
                        let end_time = schedule.substr(i + 3, 2) + ':00';
                        parsed_schedule[schedule.substr(i, 1)] =
                            {
                                start: start_time,
                                end: end_time
                            }
                    }
                    section.schedule = parsed_schedule;
                    // Adds the 'custom' section
                    sections.push(section);
                }
                return Promise.resolve(sections);
            })
    }
}
