import { Injectable } from '@angular/core';

import * as moment from 'moment';
declare var cordova: any;

import { Classes } from '../providers/classes-service';
import { Activities } from '../providers/activities-service';
import { Students } from '../providers/students-service';

@Injectable()
export class Reports {
    constructor(public classesService: Classes,
                public activitiesService: Activities,
                public studentsService: Students) {}

    generateProgressRep(section) {
        let weeks = [];
        this.classesService.getAll(section.section_id)
        .then(classes => {
            let lastTaughtClassIdx = -1;
            for(let i = 0; i < classes.length; ++i) {
                if(classes[i].type != null) lastTaughtClassIdx = i;
            }
            let weekIdx = 1;
            let firstWeek = moment(classes[0].date).isoWeek();
            let currWeek = {idx: 0, weekNum: firstWeek,
                            classes: [], acts: [], actsStr: ''};
            for(let i = 0; i < classes.length; ++i) {
                let c = classes[i];
                // Only taught classes
                if(c.type == null) continue;
                // Fetch activities by topic
                let classActs = [];
                this.activitiesService.getByTopic(section.section_id, c.topic)
                .then(acts => {
                    if(c.type == 'N') {
                        classActs = acts;
                    }
                    // Jumps to a new week if necessary
                    let classWeek = moment(c.date).isoWeek();
                    if(classWeek != currWeek.weekNum) {
                        currWeek.idx = weekIdx++;
                        // Only unique acts
                        currWeek.acts = [Array.from(new Set(currWeek.acts))];
                        currWeek.actsStr = currWeek.acts.join(', ');
                        weeks.push(currWeek);
                        currWeek = {idx: 0, weekNum: classWeek, classes: [c],
                                    acts: [classActs], actsStr: ''};
                    } else {
                        currWeek.classes.push(c);
                        currWeek.acts = currWeek.acts.concat(classActs);
                    }
                    // Last class
                    if(i == lastTaughtClassIdx) {
                        currWeek.idx = weekIdx++;
                        currWeek.acts = [Array.from(new Set(currWeek.acts))];
                        currWeek.actsStr = currWeek.acts.join(', ');
                        weeks.push(currWeek);
                        cordova.plugins.pdf.htmlToPDF({
                            data: this.getProgressHtml(weeks, section),
                            type: "share"
                        });
                    }
                })
            }
        })
    }

    getProgressHtml(weeks, section) {
        let htmlStr =`
            <html>
                <head>
                    <meta charset="utf-8">
                    <style>
                        h3, h4 {text-align: center;}
                        .table-info {margin-left: 50px;}
                        .table-info p {font-size: 14px;}
                        .table-data p {font-size: 14px;}
                        .table-data {
                            margin-top: 35px;
                            border: 2px solid black;
                            border-collapse: collapse;
                        }
                        .table-sign {
                            border-left: 2px solid black;
                            border-right: 2px solid black;
                            border-bottom: 2px solid black;
                            border-collapse: collapse;
                        }
                        th, .table-data td {border: 1px solid black;}
                        .table-info td {padding-left: 15px;}
                        .value {
                            border-bottom: 2px solid black;
                            padding-left: 5px;
                            padding-right: 5px;
                        }
                        .horiz-padd {
                            padding-left: 10px;
                            padding-right: 10px;
                            word-break: break-all;
                        }
                    </style>
                </head>
                <body>
                    <h3>
                        <strong>
                            DEPARTAMENTO DE CIENCIAS COMPUTACIONALES
                        </strong>
                    </h3>
                    <h4>
                        <strong>FORMATO DE SEGUIMIENTO DE PROGRAMA</strong>
                    </h4>`
        htmlStr += `<table class="table-info" width="85%">
                        <tr>
                            <td width="10%">
                                <p><strong>Materia</strong></p>
                            </td>
                            <td width="68%">
                                <p class="value">` + section.subject_name  + `</p>
                            </td>
                            <td width="10%" align="right">
                                <p><strong>Clave</strong></p>
                            </td>
                            <td width="12%">
                                <p class="value">` +
                                    section.subject_key
                                + `</p>
                            </td>
                        </tr>
                    </table>
                    <table class="table-info" width="85%">
                        <tr>
                            <td width="10%"><p><strong>NRC</strong></p></td>
                            <td width="12%">
                                <p class="value">` + section.nrc + `</p>
                            </td>
                            <td width="10%" align="right">
                                <p><strong>Sección</strong></p>
                            </td>
                            <td width="12%">
                                <p class="value">`
                                    + section.code_letter + '-' +
                                    section.code_number
                                + `</p>
                            </td>
                            <td width="10%" align="right">
                                <p><strong>Profesor</strong></p>
                            </td>
                            <td width="46%">
                                <p class="value">` + section.prof + `</p>
                            </td>
                        </tr>
                    </table>
                    <table class="table-data" width="90%" align="center">
                        <tr>
                            <th width="5%">
                                <p style="transform: rotate(-90.0deg);">
                                    SEMANA
                                </p>
                            </th>
                            <th width="15%" style="font-weight: normal;">
                                <p>
                                    <strong>FECHA</strong> dd/mm/aa
                                    <strong>De exposición del Tema</strong>
                                </p>
                            </th>
                            <th width="45%"><p>TEMA</p></th>
                            <th width="35%">
                                <p>ACTIVIDADES DE TEMAS VISTOS</p>
                            </th>
                        </tr>`
        let count;
        for(let w of weeks) {
            count = 0;
            for(let cl of w.classes) {
                let topic = cl.type == 'D' ? 'Descanso'
                                : cl.type == 'V' ? 'Vacaciones'
                                : cl.topic;
                htmlStr += '<tr>';
                if(count < 2) {
                    ++count;
                    htmlStr += `
                    <td width="5%" rowspan="`+ w.classes.length
                        + `" align="center">
                        <p>` + w.idx + `</p>
                    </td>`
                }
                htmlStr += `
                <td width="15%" align="center">
                    <p>` + cl.date + `</p>
                </td>
                <td width="45%">
                    <p class="horiz-padd">` + topic + `</p>
                </td>
                `
                if(count < 2) {
                    ++count;
                    htmlStr += `
                    <td width="35%" rowspan="` + w.classes.length + `">
                        <p class="horiz-padd">` +
                            w.actsStr + `
                        </p>
                    </td>`
                }
                htmlStr += '</tr>';
            }
        }
        htmlStr += `<tr>
            <td height="100px" colspan="3" align="center">
                <p><strong>FIRMA DEL PROFESOR DE TEMAS VISTOS</strong></p>
            </td>
            <td style="border-left: 1px solid black;"></td>
        </tr>
    </table>`
    return htmlStr;
    }

    generateAttendancesRep(section) {
        let classes = [];
        let studs = [];
        this.studentsService.getAll(section.section_id)
        .then(students => {
            studs = students;
            this.classesService.getActive(section.section_id)
            .then(clss => {
                classes = clss;
                for(let c of classes) {
                    this.classesService.getNumOfAtts(section.section_id,
                                                     c.date)
                    .then(numAtts => {
                        c.percent = numAtts * 1.0 / (students.length * c.num_sessions) * 100;
                    })
                }
            })
            .then(() => {
                for(let i = 0; i < studs.length; ++i) {
                    this.studentsService.getAttendances(studs[i].enrolled_id)
                    .then(atts => {
                        studs[i].attendances = atts;
                        if(i == studs.length - 1) {
                            cordova.plugins.pdf.htmlToPDF({
                                data: this.getAttendancesHtml(section, classes,
                                                              studs),
                                landscape: "landscape",
                                type: "share"
                            });
                        }
                    })
                }
            })
        })
    }

    getAttendancesHtml(section, classes, students) {
        let htmlString = `<html>
        <head>
        <style>
        table, th, td {
            border: 1px solid black;
        }
        </style>
        </head>
        <body>`
        htmlString += "<h4>Materia: " + section.subject_name +
                        " Clave: " + section.subject_key + " NRC: " +
                        section.nrc + " Ciclo: " + section.calendar_year +
                        section.calendar_letter + "</h4>";
        htmlString += `
        <table style="width:50%">
          <tr>
              <th>Estudiante / Fecha</th>`;
        for (let cls of classes) {
            htmlString += "<th>" + cls.date + "</th>";
        }
        htmlString += "<th>% Total</th>";
        htmlString += "<th>Estado</th>";
        htmlString +=  `</tr>`;
        for (let stud of students)
        {
            htmlString += "<tr><td>" + stud.student_code + " " + stud.name + "</td>";
            for (let att of stud.attendances)
            {
                htmlString += "<td>" + att.type + "</td>";
            }
            htmlString += "<td>" + stud.att_percent + "</td>";
            let state = stud.att_percent < 65 ? 'SD' : stud.att_percent < 80 ? 'EXT' : 'ORD';
            htmlString += "<td>" + state + "</td>";
            htmlString += "</tr>";
        }
        htmlString += "<tr><td>Total</td>";
        let totalPercent = 0;
        for(let c of classes) {
            htmlString += "<td>" + c.percent + "</td>";
            totalPercent += c.percent;
        }
        htmlString += "<td>" + totalPercent / classes.length + "</td></tr>";
        htmlString += `</table>

                        </body>
                        </html>`;
        return htmlString;
    }

    generateGradesRep(section) {
        let studs = [];
        this.studentsService.getAll(section.section_id)
        .then(students => {
            studs = students;
            for(let i = 0; i < studs.length; ++i) {
                this.studentsService.getDeliveries(studs[i].enrolled_id)
                .then(delivs => {
                    studs[i].delivs = delivs;
                    if(i == studs.length - 1) {
                        cordova.plugins.pdf.htmlToPDF({
                            data: this.getGradesHtml(section, studs),
                            landscape: "landscape",
                            type: "share"
                        });
                    }
                })
            }
        })
    }

    getGradesHtml(section, students) {
        let htmlString = `<!DOCTYPE html>
                        <html>
                        <head>
                        <style>
                        table, th, td {
                            border: 1px solid black;
                        }
                        </style>
                        </head>
                        <body>`
        htmlString += "<h4>Materia: " + section.subject_name +
                        " Clave: " + section.subject_key + " NRC: " +
                        section.nrc + " Ciclo: " + section.calendar_year +
                        section.calendar_letter + "</h4>";
        htmlString += `
                        <table style="width:50%">`;
        htmlString += `<tr>
                              <th>Estudiante / Actividad</th>`;
        if(students.length) {
            for(let d of students[0].delivs) {
                htmlString += "<th>" + d.name + "</th>";
            }
        }
        htmlString += "<th>Estado</th>";
        htmlString += "<th>Ordinario</th>";
        htmlString += "<th>Extraordinario</th>";
        htmlString += "<th>Calificacion Final</th>";
        htmlString +=  `</tr>`;
        for(let stud of students) {
            htmlString += "<tr><td>" + stud.student_code + " " + stud.name + "</td>";
            for(let deliv of stud.delivs) {
                htmlString += "<td>" + deliv.score + "</td>";
            }
            let state = stud.att_percent < 65 ? 'SD' : stud.ext_score != null ? 'EXT' : 'ORD';
            htmlString += "<td>" + state + "</td>";
            htmlString += "<td>" + stud.ord_grade + "</td>";
            let extra = stud.ext_score == null ? '-' : stud.ext_score;
            htmlString += "<td>" + extra + "</td>";
            htmlString += "<td>" + stud.grade + "</td>";
            htmlString += "</tr>";
        }
        htmlString += `</table>

                        </body>
                        </html>`;
        return htmlString;
    }
}
