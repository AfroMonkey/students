import { Injectable } from '@angular/core';
import { Database } from '../providers/database-service';

@Injectable()
export class Subjects {
    constructor(public db: Database) {}

    add(subject) {
        let sqlStmt = 'INSERT INTO subjects(subject_key, name) VALUES(?, ?)';
        return this.db.query(sqlStmt, [subject.subject_key, subject.name]);
    }

    update(subject_key, subject) {
        let sqlStmt = `UPDATE subjects
                       SET subject_key = ?, name = ?
                       WHERE subject_key = ?`;
        return this.db.query(sqlStmt, [subject.subject_key, subject.name,
                                       subject_key]);
    }

    delete(subject_key) {
        let sqlStmt = 'DELETE FROM subjects WHERE subject_key = ?';
        return this.db.query(sqlStmt, [subject_key]);
    }

    exists(col, val) {
        let sqlStmt = 'SELECT * FROM subjects WHERE ' + col + ' = ? LIMIT 1';
        return this.db.query(sqlStmt, [val])
            .then(response => {
                return Promise.resolve(response.rows.length);
            })
    }

    name(subject_key) {
        let sqlStmt = 'SELECT name FROM subjects WHERE subject_key = ?';
        return this.db.query(sqlStmt, [subject_key])
            .then(response => {
                return Promise.resolve(response.rows.length ?
                                       response.rows.item(0).name : '')
            })
    }

    getAll() {
        let sqlStmt = 'SELECT * FROM subjects ORDER BY name ASC';
        return this.db.query(sqlStmt, [])
            .then(response => {
                let subjects = [];
                for(let index = 0; index < response.rows.length; ++index) {
                    subjects.push(response.rows.item(index));
                }
                return Promise.resolve(subjects);
            })
    }
}
