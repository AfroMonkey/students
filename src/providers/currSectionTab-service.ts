import { Injectable } from '@angular/core';

@Injectable()
export class CurrSectionTab {
    active: String;

    constructor() {}

    setActive(tabStr: String) {
        this.active = tabStr;
    }

    getActive() {
        return this.active;
    }
}
