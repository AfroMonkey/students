import { Component, ViewChild } from '@angular/core';
import { Platform, MenuController, Nav, ModalController,
         Events } from 'ionic-angular';
import { StatusBar, Splashscreen } from 'ionic-native';

import { SubjectsPage } from '../pages/subjects/subjects';
import { SectionPage } from '../pages/section/section';
import { ImportPage } from '../pages/import/import';

import { Database } from '../providers/database-service';
import { Subjects } from '../providers/subjects-service';
import { Students } from '../providers/students-service';
import { CurrSection } from '../providers/currSection-service';
import { Utils } from '../providers/utils-service';
import { Reports } from '../providers/reports-service';

@Component({
    templateUrl: 'app.html'
})

export class MyApp {
    @ViewChild(Nav) nav: Nav;

    rootPage: any;
    pages: Array < {title: string, component: any} >;

    calendars = ['2015B', '2016A', '2016B', '2017A'];
    currCalendar = this.calendars[this.calendars.length - 1];

    constructor(platform: Platform, private menu: MenuController,
                public modalCtrl: ModalController, public events: Events,
                public db: Database, public crrSct: CurrSection,
                public subjectsServie: Subjects,
                public studentsService: Students,
                public reportsService: Reports) {
        this.initializeApp(platform);
        this.pages = [
                        {title: 'Materias', component: SubjectsPage}
                     ];
    }

    initializeApp(platform: Platform) {
        platform.ready().then(() => {
            StatusBar.styleDefault();
            Splashscreen.hide();
            this.db.open()
                .then(() => this.db.setupScheme())
                    .then(() => this.rootPage = SubjectsPage);
        });
    }

    getSchKeys(schedule) {
        return Utils.getScheduleKeys(schedule);
    }

    openPage(page) {
        this.menu.close();
        this.nav.setRoot(page.component);
    }

    openNow() {
        this.menu.close();
        this.nav.setRoot(SectionPage, {sectionData : {subject_key: 'I5890', code: 'D-05',
        days: 'Lunes: 13-15\nMiércoles: 13-15'}});
    }

    private isStudCode(str) {
        if(str.length != 9) return false;
        let chars = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
        for(let ch of str) {
            if(!(ch in chars)) {
                return false;
            }
        }
        return true;
    }

    importStudents() {
        this.menu.close();

        let modal = this.modalCtrl.create(ImportPage);
        modal.onDidDismiss(data => {
            if(data) {
                let section_id = this.crrSct.data().section_id;
                let studs = data.studentsStr.split('\n');
                for(let s of studs) {
                    let studData = s.split(',');
                    let code = studData[0].trim();
                    let name = studData[1].trim();
                    // Code and name are valid
                    if(this.isStudCode(code) && name.length) {
                        let exists = false;
                        // Checks if the student exists
                        this.studentsService.name(code)
                        .then(studName => {
                            if(studName) {
                                exists = true;
                            }
                            this.studentsService.isEnrolled(code, section_id)
                            .then(enrolled => {
                                if(!enrolled) {
                                    let newStud = {student_code: code,
                                                   name: name}
                                    this.studentsService.add(newStud,
                                                             section_id, exists)
                                    .then(added => {
                                        this.events.publish('studentUpdated');
                                    })
                                }
                            })
                        })
                    }
                }
            }
        })
        modal.present();
    }

    createProgressReport() {
        this.menu.close();

        let section = this.crrSct.data();
        section.prof = 'Macías Brambila Hassem Rubén';
        this.subjectsServie.name(section.subject_key)
        .then(name => {
            section.subject_name = name;
            this.reportsService.generateProgressRep(section);
        })
    }

    createAttendanceReport() {
        this.menu.close();

        let section = this.crrSct.data();
        this.subjectsServie.name(section.subject_key)
        .then(name => {
            section.subject_name = name;
            this.reportsService.generateAttendancesRep(section);
        })
    }

    createGradesReport() {
        this.menu.close();

        let section = this.crrSct.data();
        this.subjectsServie.name(section.subject_key)
        .then(name => {
            section.subject_name = name;
            this.reportsService.generateGradesRep(section);
        })
    }
}
