import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { SectionPage } from '../pages/section/section';
import { SectionFormPage } from '../pages/section-form/section-form';
import { ScheduleFormPage } from '../pages/schedule-form/schedule-form';
import { SubjectsPage } from '../pages/subjects/subjects';
import { SubjectPage } from '../pages/subject/subject';
import { SubjectFormPage } from '../pages/subject-form/subject-form';
import { AttendancePage } from '../pages/attendance/attendance';
import { StudentsPage } from '../pages/students/students';
import { StudentPage } from '../pages/student/student';
import { ExtraordinaryPage } from '../pages/extraordinary/extraordinary';
import { StudentAttendancePage } from '../pages/student-attendance/student-attendance';
import { StudentActivitiesPage } from '../pages/student-activities/student-activities';
import { StudentFormPage } from '../pages/student-form/student-form';
import { RubricsPage } from '../pages/rubrics/rubrics';
import { RubricFormPage } from '../pages/rubric-form/rubric-form';
import { ActivitiesPage } from '../pages/activities/activities';
import { ActivityFormPage } from '../pages/activity-form/activity-form';
import { DeliveriesPage } from '../pages/deliveries/deliveries';
import { ScoreFormPage } from '../pages/score-form/score-form';
import { ClassesPage } from '../pages/classes/classes';
import { ClassFormPage } from '../pages/class-form/class-form';
import { ImportPage } from '../pages/import/import';

import { Database } from '../providers/database-service';
import { Subjects } from '../providers/subjects-service';
import { Sections } from '../providers/sections-service';
import { Rubrics } from '../providers/rubrics-service';
import { Classes } from '../providers/classes-service';
import { Students } from '../providers/students-service';
import { Activities } from '../providers/activities-service';
import { Deliveries } from '../providers/deliveries-service';
import { Attendances } from '../providers/attendances-service';
import { Extraordinary } from '../providers/extraordinary-service';
import { CurrSection } from '../providers/currSection-service';
import { CurrSectionTab } from '../providers/currSectionTab-service';
import { Formater } from '../providers/format-service';
import { Utils } from '../providers/utils-service';
import { Reports } from '../providers/reports-service';

@NgModule({
  declarations: [
    MyApp,
    SectionPage,
    SectionFormPage,
    ScheduleFormPage,
    SubjectsPage,
    SubjectPage,
    SubjectFormPage,
    AttendancePage,
    StudentsPage,
    StudentPage,
    ExtraordinaryPage,
    StudentAttendancePage,
    StudentActivitiesPage,
    StudentFormPage,
    RubricsPage,
    RubricFormPage,
    ActivitiesPage,
    ActivityFormPage,
    DeliveriesPage,
    ScoreFormPage,
    ClassesPage,
    ClassFormPage,
    ImportPage
  ],

  imports: [
    IonicModule.forRoot(
        MyApp,
        {
            monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
                         'Julio', 'Agosto', 'Septiembre', 'Octubre',
                         'Noviembre', 'Diciembre']
        }
    )
  ],

  bootstrap: [IonicApp],

  entryComponents: [
    MyApp,
    SectionPage,
    SectionFormPage,
    ScheduleFormPage,
    SubjectsPage,
    SubjectPage,
    SubjectFormPage,
    AttendancePage,
    StudentsPage,
    StudentPage,
    ExtraordinaryPage,
    StudentAttendancePage,
    StudentActivitiesPage,
    StudentFormPage,
    RubricsPage,
    RubricFormPage,
    ActivitiesPage,
    ActivityFormPage,
    DeliveriesPage,
    ScoreFormPage,
    ClassesPage,
    ClassFormPage,
    ImportPage
  ],

  providers: [
      {provide: ErrorHandler, useClass: IonicErrorHandler},
      Database,
      Subjects,
      Sections,
      Rubrics,
      Classes,
      Students,
      Activities,
      Deliveries,
      Attendances,
      Extraordinary,
      CurrSection,
      CurrSectionTab,
      Formater,
      Utils,
      Reports
  ]
})

export class AppModule {}
